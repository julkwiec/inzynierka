﻿using Serializer.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Serializer.Helpers
{
    class BitVector
    {
        private ByteVector vector;

        public BitVector(ByteVector vector)
        {
            this.vector = vector;
        }

        public bool this[int index]
        {
            get => ReadAtCoords(GetBitCoords(index));
            set => WriteAtCoords(GetBitCoords(index), value);            
        }

        public int GetSignificantBitCount()
        {
            int bitCount = 0;
            bool onlyZeros = true;
            for(int i = 0; i < vector.Count; i++)
            {
                if (vector[i] == 0 && onlyZeros) continue;
                if (onlyZeros)
                {
                    byte tmp = vector[i];
                    while(tmp > 0)
                    {
                        tmp >>= 1;
                        bitCount++;
                    }
                }
                else
                {
                    bitCount += 8;
                }
                onlyZeros = false;
            }
            return Math.Max(bitCount, 1);
        }

        private bool ReadAtCoords(BitCoords coords)
        {
            return vector[coords.ByteIndex].ReadBit(coords.BitIndex);
        }

        private void WriteAtCoords(BitCoords coords, bool value)
        {
            vector[coords.ByteIndex] = vector[coords.ByteIndex].WriteBit(coords.BitIndex, value);
        }

        public byte ReadSegmentAt(int startIndex, int count)
        {
            if (count > 7) throw new ArgumentOutOfRangeException();
            byte value = 0;
            for (int i = startIndex; i < startIndex + count; i++)
            {
                var coords = GetBitCoords(i);
                var @byte = vector[coords.ByteIndex];
                var bit = @byte.ReadBit(coords.BitIndex) ? 1 : 0;
                value = (byte)((value << 1) | bit);
            }

            return value;
        }

        private BitCoords GetBitCoords (int bitIndex)
        {
            return new BitCoords(bitIndex / 8, bitIndex % 8);
        }

        private struct BitCoords
        {
            public int ByteIndex { get; }
            public int BitIndex { get; }

            public BitCoords(int byteIndex, int bitIndex)
            {
                ByteIndex = byteIndex;
                BitIndex = bitIndex;
            }
        }
    }
}
