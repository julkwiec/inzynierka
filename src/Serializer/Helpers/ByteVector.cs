﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Serializer.Helpers
{
    public class ByteVector : IList<byte>, IEnumerable<byte>, ICollection<byte>
    {
        private const int DefaultCapacity = 4;
        private static byte[] emptyArray = new byte[0];
        private byte[] buffer;
        private int currentLength = 0;

        public byte[] UnderlyingBuffer => buffer;

        public ByteVector()
        {
            buffer = emptyArray;
        }

        public ByteVector(int initialCapacity)
        {
            buffer = new byte[initialCapacity];
        }

        public void ReverseIfLittleEndian()
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(buffer, 0, currentLength);
            }
        }

        public ByteVector(byte[] array) : this(array, array.Length) { }
        public ByteVector(byte[] array, int length)
        {
            buffer = array;
            currentLength = length;
        }

        public void SetLength(int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException();
            EnsureCapacity(length);
            currentLength = length;
        }

        public ArraySegment<byte> ToSegment()
        {
            return new ArraySegment<byte>(buffer, 0, currentLength);
        }

        public byte this[int index]
        {
            get { ThrowIfOutOfBounds(index); return buffer[index]; }
            set { ThrowIfOutOfBounds(index); buffer[index] = value; }
        }

        public int Length => currentLength;
        public int Count => currentLength;

        public bool IsReadOnly => false;

        public void Add(byte item)
        {
            var index = currentLength;
            currentLength++;
            EnsureCapacity(currentLength);
            buffer[index] = item;
        }

        public void AddRange(ArraySegment<byte> array) => AddRange(array.Array, array.Offset, array.Count);
        public void AddRange(byte[] array) => AddRange(array, 0, array.Length);
        public void AddRange(byte[] array, int offset, int count)
        {
            EnsureCapacity(currentLength + count);
            Buffer.BlockCopy(array, offset, buffer, currentLength, count);
            currentLength += count;
        }

        public void AddRange(IEnumerable<byte> bytes)
        {
            foreach (var b in bytes)
            {
                Add(b);
            }
        }

        public void Clear()
        {
            currentLength = 0;
        }

        public bool Contains(byte item)
        {
            for(int i = 0; i < currentLength; i++)
            {
                if (buffer[i] == item) return true;
            }
            return false;
        }

        public void CopyTo(byte[] array, int arrayIndex)
        {
            for(int i = 0; i < currentLength; i++)
            {
                array[arrayIndex + i] = buffer[i];
            }
        }

        public IEnumerator<byte> GetEnumerator()
        {
            return new VectorEnumerator(this);
        }

        public int IndexOf(byte item)
        {
            for(int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] == item) return i;
            }
            return -1;
        }

        public void Insert(int index, byte item)
        {
            if (index < 0 || index > currentLength) throw new IndexOutOfRangeException();
            currentLength++;
            EnsureCapacity(currentLength);
            for (int i = currentLength - 1; i > index; i--)
            {
                buffer[i] = buffer[i - 1];
            }
            buffer[index] = item;
        }

        public bool Remove(byte item)
        {
            var index = IndexOf(item);
            if (index < 0) return false;
            RemoveAt(index);
            return true;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= currentLength) throw new IndexOutOfRangeException();
            for(int i = index; i < currentLength; i++)
            {
                buffer[i] = buffer[i + 1];
            }
            currentLength--;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new VectorEnumerator(this);
        }

        private void EnsureCapacity(int cap)
        {
            var newCap = buffer.Length == 0 ? DefaultCapacity : buffer.Length * 2;
            if (cap > newCap)
            {
                newCap = cap;
            }
            var newBuffer = new byte[newCap];
            if (buffer.Length > 0)
            {
                Buffer.BlockCopy(buffer, 0, newBuffer, 0, buffer.Length);
            }
            buffer = newBuffer;
        }

        private void ThrowIfOutOfBounds(int index)
        {
            if (index > currentLength - 1) throw new IndexOutOfRangeException();
        }

        private class VectorEnumerator : IEnumerator, IEnumerator<byte>
        {
            private int index = -1;
            private ByteVector vector;

            public VectorEnumerator(ByteVector vector)
            {
                this.vector = vector;
            }

            object IEnumerator.Current => ((IEnumerator<byte>)this).Current;
            byte IEnumerator<byte>.Current
            {
                get
                {
                    if (index < 0 || index >= vector.currentLength) throw new InvalidOperationException();
                    return vector[index];
                }
            }

            public void Dispose()
            {
                
            }

            public bool MoveNext()
            {
                index++;
                return index < vector.currentLength;
            }

            public void Reset()
            {
                index = -1;
            }
        }
    }
}
