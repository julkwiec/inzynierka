using Serializer.Extensions;
using System;
using System.Linq;
using System.Reflection;

namespace Serializer.Helpers

{
    internal static class UniCast
    {
        public static object ChangeType(object item, Type destinationType)
        {
            if (item == null || item == DBNull.Value)
            {
                return destinationType == typeof(DBNull) ? DBNull.Value : null;
            }

            var itemType = item.GetType();
            var itemTypeInfo = itemType.GetTypeInfo();
            if (itemType == typeof(string))
            {
                object result;
                if (TryStringToEnum(item, destinationType, out result))
                {
                    return result;
                }

            }
            else if (itemTypeInfo.IsClass)
            {
                return ReflectionCast(item, destinationType);
            }

            var tmpItem = new CastItem(item, itemType);
            tmpItem = TryUnpackNullable(tmpItem);
            tmpItem = TryUnpackEnum(tmpItem);
            item = tmpItem.Value;
            return Pack(item, destinationType);
        }

        private static CastItem TryUnpackEnum(CastItem item)
        {
            if (!item.Type.GetTypeInfo().IsEnum) return item;
            var type = Enum.GetUnderlyingType(item.Type);
            var value = Convert.ChangeType(item.Value, type);
            return new CastItem(value, type);
        }

        private static CastItem TryUnpackNullable(CastItem item)
        {
            var type = Nullable.GetUnderlyingType(item.Type);
            if (type == null) return item;
            var value = Convert.ChangeType(item.Value, type);
            return new CastItem(value, type);
        }

        private static object Pack(object item, Type type)
        {
            if (item == null || item == DBNull.Value) return null;
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.IsClass) return ReflectionCast(item, type);
            var underlyingNullableType = Nullable.GetUnderlyingType(type);
            var isNullable = underlyingNullableType != null;
            var isEnum = isNullable ? underlyingNullableType.GetTypeInfo().IsEnum : typeInfo.IsEnum;

            if (isEnum)
            {
                if (isNullable)
                {
                    var enumUnderlyingType = Enum.GetUnderlyingType(underlyingNullableType);
                    var tmp = ConvertType(item, enumUnderlyingType);
                    tmp = Enum.ToObject(underlyingNullableType, tmp);
                    return tmp;
                }
                else
                {
                    var enumUnderlyingType = Enum.GetUnderlyingType(type);
                    var tmp = ConvertType(item, enumUnderlyingType);
                    tmp = Enum.ToObject(type, tmp);
                    return tmp;
                }
            }
            else
            {
                if (isNullable)
                {
                    var tmp = ConvertType(item, underlyingNullableType);
                    return tmp;
                }
                else
                {
                    var tmp = ConvertType(item, type);
                    return tmp;
                }
            }
        }

        private static object ConvertType(object item, Type type)
        {
            if (type.GetTypeInfo().IsInstanceOfType(item))
            {
                return item;
            }
            try
            {
                if (item is IConvertible)
                {
                    return Convert.ChangeType(item, type);
                }
            }
            catch
            {
            }
            return ReflectionCast(item, type);
        }

        private static bool TryStringToEnum(object item, Type destinationType, out object result)
        {
            var enumType = GetEnumType(destinationType);
            string str = item as string;
            if (str != null && enumType != null)
            {
                result = Pack(StringToEnum(str, enumType), destinationType);
                return true;
            }
            result = null;
            return false;
        }

        private static object StringToEnum(string str, Type destinationType)
        {
            if (str.IsNumericString())
            {
                var enumUnderlyingType = Enum.GetUnderlyingType(destinationType);
                object enumValue = Convert.ChangeType(str, enumUnderlyingType);
                return Enum.ToObject(destinationType, enumValue);
            }
            return Enum.Parse(destinationType, str.Trim(), true);
        }

        private static Type GetEnumType(Type type)
        {
            Type ret = Nullable.GetUnderlyingType(type) ?? type;
            return ret.GetTypeInfo().IsEnum ? ret : null;
        }

        private static object ReflectionCast(object item, Type type)
        {
            var castMethod = item.GetType().GetTypeInfo()
                .GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic |
                                BindingFlags.FlattenHierarchy)
                .Where(m => m.Name == "op_Implicit" || m.Name == "op_Explicit")
                .FirstOrDefault(m => m.ReturnType == type);
            return castMethod == null ? item : castMethod.Invoke(item, null);
        }

        private struct CastItem
        {
            public object Value { get; }
            public Type Type { get; }

            public CastItem(object value, Type type)
            {
                Value = value;
                Type = type;
            }
        }
    }
}