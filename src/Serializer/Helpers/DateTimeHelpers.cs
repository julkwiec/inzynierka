﻿using NodaTime;
using Serializer.Headers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Serializer.Helpers
{
    static class DateTimeHelpers
    {
        public static long TickOfSecond(this DateTime dateTime)
        {
            var diff = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
            var ts = dateTime - diff;
            return ts.Ticks;
        }

        public static long TickOfSecond(this DateTimeOffset dateTime)
        {
            var diff = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
            var ts = dateTime - diff;
            return ts.Ticks;
        }

        public static long TickOfSecond(this TimeSpan timeSpan)
        {
            var diff = new TimeSpan(timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
            return (timeSpan - diff).Ticks;
        }

        public static DateTimePrecision GetNeededPrecision(this DateTime dateTime)
        {
            if (dateTime.TickOfSecond() != 0) return DateTimePrecision.Tick;
            if (dateTime.Second != 0) return DateTimePrecision.Second;
            if (dateTime.Minute != 0) return DateTimePrecision.Minute;
            return DateTimePrecision.Day;
        }

        public static DateTimePrecision GetNeededPrecision(this DateTimeOffset dateTime)
        {
            if (dateTime.TickOfSecond() != 0) return DateTimePrecision.Tick;
            if (dateTime.Second != 0) return DateTimePrecision.Second;
            if (dateTime.Minute != 0) return DateTimePrecision.Minute;
            return DateTimePrecision.Day;
        }

        public static DateTimePrecision GetNeededPrecision(this LocalDateTime dateTime)
        {
            if (dateTime.TickOfSecond != 0) return DateTimePrecision.Tick;
            if (dateTime.Second != 0) return DateTimePrecision.Second;
            if (dateTime.Minute != 0) return DateTimePrecision.Minute;
            return DateTimePrecision.Day;
        }

        public static DateTimePrecision GetNeededPrecision(this OffsetDateTime dateTime)
        {
            if (dateTime.TickOfSecond != 0) return DateTimePrecision.Tick;
            if (dateTime.Second != 0) return DateTimePrecision.Second;
            if (dateTime.Minute != 0) return DateTimePrecision.Minute;
            return DateTimePrecision.Day;
        }
    }
}
