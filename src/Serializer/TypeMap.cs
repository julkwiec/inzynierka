﻿using System;
using System.Collections.Generic;
using System.Reflection;
using TypeCode = Serializer.Headers.TypeCode;
using System.Collections;
using System.Numerics;
using NodaTime;
using Serializer.Extensions;

namespace Serializer
{
    static class TypeMap
    {
        public static TypeCode MapTypeToCode(Type type)
        {
            if (IsNull(type)) return TypeCode.Null;
            if (IsBool(type)) return TypeCode.Bool;
            if (IsBinaryNumber(type)) return TypeCode.BinaryNumber;
            if (IsDecimalNumber(type)) return TypeCode.DecimalNumber;
            if (IsString(type)) return TypeCode.String;
            if (IsLocalDateTime(type)) return TypeCode.LocalDateTime;
            if (IsOffsetDateTime(type)) return TypeCode.OffsetDateTime;
            if (IsTimespan(type)) return TypeCode.TimeSpan;
            if (IsDictionary(type)) return TypeCode.Map;
            if (IsList(type)) return TypeCode.List;
            return TypeCode.Map;
        }

        public static TypeCode MapObjectToCode(Object obj) => MapTypeToCode(obj?.GetType());

        private static bool IsBinaryNumber(Type type)
        {
            type = type.Unwrap();
            return
                type == typeof(sbyte) ||
                type == typeof(byte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(BigInteger) ||
                type == typeof(float) ||
                type == typeof(double);
        }

        private static bool IsDecimalNumber(Type type)
        {
            return type == typeof(decimal) || type == typeof(decimal?);
        }

        private static bool IsDictionary(Type type)
        {
            type = type.Unwrap();
            return typeof(IDictionary).GetTypeInfo().IsAssignableFrom(type);
        }

        private static bool IsBool(Type type)
        {
            return type == typeof(bool) || type == typeof(bool?);
        }

        private static bool IsNull(Type type)
        {
            return type == null || type == typeof(DBNull);
        }

        private static bool IsLocalDateTime(Type type)
        {
            return
                type == typeof(DateTime) || type == typeof(LocalDateTime) ||
                type == typeof(DateTime?) || type == typeof(LocalDateTime?);
        }

        private static bool IsOffsetDateTime(Type type)
        {
            return 
                type == typeof(DateTimeOffset) || type == typeof(OffsetDateTime) ||
                type == typeof(DateTimeOffset?) || type == typeof(OffsetDateTime?);
        }

        private static bool IsString(Type type)
        {
            return type == typeof(string) || type == typeof(byte[]);
        }

        private static bool IsTimespan(Type type)
        {
            return
                type == typeof(TimeSpan) || type == typeof(Period) ||
                type == typeof(TimeSpan?);
        }

        private static bool IsList(Type type)
        {
            if (type.IsArray) return true;
            if (type.IsConstructedGenericType)
            {
                var genericType = type.GetTypeInfo().GetGenericTypeDefinition();
                if (typeof(IEnumerable<>).GetTypeInfo().IsAssignableFrom(genericType) ||
                    typeof(ICollection<>).GetTypeInfo().IsAssignableFrom(genericType) ||
                    typeof(IList<>).GetTypeInfo().IsAssignableFrom(genericType))
                {
                    return true;
                }
            }
            if (typeof(IEnumerable).GetTypeInfo().IsAssignableFrom(type) ||
                typeof(ICollection).GetTypeInfo().IsAssignableFrom(type) ||
                typeof(IList).GetTypeInfo().IsAssignableFrom(type))
            {
                return true;
            }
            return false;
        }
    }
}
