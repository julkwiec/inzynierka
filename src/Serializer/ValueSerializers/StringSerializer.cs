﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Encoding;

namespace Serializer.ValueSerializers
{
    class StringSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.String;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new StringHeader(header);
            var byteCount = (int)VariableLengthEncoding.DecodeUint(stream);
            var result = hdr.StringType == StringType.Bytes
                ? (object) ReadArray(stream, byteCount)
                : (object) ReadString(stream, byteCount, GetEncoding(hdr.StringType));
            context.TryUpdateDeserializationCache(WireType, result);
            return result;
        }

        private System.Text.Encoding GetEncoding(StringType type)
        {
            switch (type)
            {
                case StringType.UTF8: return new UTF8Encoding(false, true);
                default: throw new InvalidOperationException();
            }
        }

        private string ReadString(Stream stream, int byteCount, System.Text.Encoding encoding)
        {
            return encoding.GetString(ReadArray(stream, byteCount));
        }

        private byte[] ReadArray(Stream stream, int byteCount)
        {
            using (var reader = new BinaryReader(stream, System.Text.Encoding.ASCII, true))
            {
                return reader.ReadBytes(byteCount);
            }
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var maybeString = value as string;
            if (maybeString != null)
            {
                WriteString(maybeString, stream);
                return;
            }

            var maybeBytes = value as byte[];
            if (maybeBytes != null)
            {
                WriteBytes(maybeBytes, stream);
                return;
            }
            throw new InvalidOperationException();
        }

        private void WriteString(string value, Stream stream)
        {
            var hdr = StringHeader.FromFields(StringType.UTF8);
            stream.WriteByte(hdr.GetValue());
            var encoding = GetEncoding(hdr.StringType);
            var bytes = encoding.GetBytes(value);
            VariableLengthEncoding.Encode(stream, bytes.Length);
            stream.Write(bytes, 0, bytes.Length);
        }

        private void WriteBytes(byte[] bytes, Stream stream)
        {
            var hdr = StringHeader.FromFields(StringType.Bytes);
            stream.WriteByte(hdr.GetValue());
            VariableLengthEncoding.Encode(stream, bytes.Length);
            stream.Write(bytes, 0, bytes.Length);
        }
    }
}
