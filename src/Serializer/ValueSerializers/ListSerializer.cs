﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Encoding;
using System.Reflection;
using System.Collections;
using Serializer.Helpers;

namespace Serializer.ValueSerializers
{
    class ListSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.List;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var count = (int)VariableLengthEncoding.DecodeUint(stream);
            var collectionWrapper = new CollectionWrapper(destinationType, count);
            context.TryUpdateDeserializationCache(WireType, collectionWrapper.GetCollection());
            FillCollection(collectionWrapper, stream, context);
            return collectionWrapper.GetCollection();
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var enumerable = (IEnumerable)value;
            int count = 0;
            foreach(var item in enumerable)
            {
                count++;
            }
            var header = ListHeader.FromFields();
            stream.WriteByte(header.GetValue());
            VariableLengthEncoding.Encode(stream, count);
            foreach (var item in enumerable)
            {
                context.Serialize(stream, item);
            }
        }

        private void FillCollection(CollectionWrapper collectionWrapper, Stream stream, SerializationContext context)
        {
            for(int i = 0; i < collectionWrapper.Count; i++)
            {
                var obj = context.Deserialize(stream, collectionWrapper.ElementType);
                collectionWrapper.Add(obj);
            }
        }
        
        private class CollectionWrapper
        {
            public Action<object> Add { get; private set; }
            private int index = 0;
            private object collection;

            public object GetCollection() => collection;
            public int Count { get; }
            public Type ElementType { get; }

            public CollectionWrapper(Type type, int length)
            {
                Count = length;
                if (!typeof(IEnumerable).GetTypeInfo().IsAssignableFrom(type))
                {
                    throw new InvalidOperationException($"Could not create a collection compatible with type {type}");
                }
                if (type.IsArray)
                {
                    ElementType = type.GetElementType();
                    var array = Array.CreateInstance(ElementType, length);
                    collection = array;
                    Add = item =>
                    {
                        var newItem = UniCast.ChangeType(item, ElementType);
                        array.SetValue(newItem, index);
                        index++;
                    };
                    return;
                }

                ElementType = type.GetTypeInfo().IsGenericType
                    ? type.GetTypeInfo().GetGenericArguments()[0]
                    : typeof(object);

                Type collectionConcreteType = type.GetTypeInfo().IsInterface
                    ? typeof(List<>).MakeGenericType(ElementType)
                    : type;

                var instance = (IList)Activator.CreateInstance(collectionConcreteType);
                collection = instance;
                Add = item => instance.Add(item);
                return;
            }
        }
    }
}
