﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.ValueSerializers
{
    class BoolSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.Bool;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            if (destinationType != typeof(bool))
            {
                ThrowMappingError(TypeCode.Bool, destinationType);
            }
            return new BoolHeader(header).Value;
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            if (!(value is bool))
            {
                ThrowReverseMappingError(value.GetType(), TypeCode.Bool);
            }

            var header = BoolHeader.FromFields((bool)value);
            stream.WriteByte(header.GetValue());
        }
    }
}
