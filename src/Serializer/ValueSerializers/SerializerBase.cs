﻿using System;
using System.IO;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.ValueSerializers
{
    public abstract class SerializerBase
    {
        public abstract TypeCode WireType { get; }

        public abstract object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context);
        public abstract void Serialize(object value, Stream stream, SerializationContext context);

        protected void ThrowMappingError(object from, Type to)
        {
            throw new InvalidOperationException($"Cannot map encoded type {from} to .NET type {to.Name}");
        }

        protected void ThrowReverseMappingError(Type from, object to)
        {
            throw new InvalidOperationException($"Cannot map .NET type {from.Name} to encoded type {to}");
        }
    }
}