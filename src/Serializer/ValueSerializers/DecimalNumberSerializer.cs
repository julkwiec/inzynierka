﻿using System;
using System.IO;
using Serializer.Headers;
using Serializer.Extensions;
using Serializer.Encoding;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Helpers;
using System.Numerics;

namespace Serializer.ValueSerializers
{
    public class DecimalNumberSerializer : SerializerBase
    {
        public override TypeCode WireType { get; } = TypeCode.DecimalNumber;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            if (destinationType != typeof(decimal))
            {
                ThrowMappingError(TypeCode.DecimalNumber, destinationType);
            }

            var hdr = new DecimalNumberHeader(header);
            var mantissa = VariableLengthEncoding.DecodeBigInteger(stream);
            var exponent = VariableLengthEncoding.DecodeBigInteger(stream);

            var number = ((decimal)mantissa) * SignToNumber(hdr.IsMantissaNegative);
            var multiplier = GetMultiplier(((decimal)exponent), hdr.IsExponentNegative);
            var result = number * multiplier;
            return result;
        }

        private long SignToNumber(bool isNegative) => isNegative ? -1 : 1;

        private decimal GetMultiplier(decimal exponent, bool isNegative)
        {
            decimal value = 1;
            for(ulong i = 0; i < exponent; i++)
            {
                value *= 10;
            }
            if (isNegative)
            {
                value = 1M / value;
            }
            return value;
        }
        
        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            if (!(value is Decimal))
            {
                throw new ArgumentException($"This serializer cannot serialize type {value.GetType().Name}.");
            }
            var decimalValue = (decimal)value;
            var isMantissaNegative = decimalValue < 0;
            var isExponentNegative = decimalValue % 1M != 0;
            decimalValue = isMantissaNegative ? -decimalValue : decimalValue;
            var header = DecimalNumberHeader.FromFields(isMantissaNegative, isExponentNegative);
            long exponent = 0;
            if (isExponentNegative)
            {
                while(decimalValue % 1M != 0)
                {
                    exponent++;
                    decimalValue *= 10;
                }
            }
            else if (decimalValue == 0)
            {
                exponent = 1;
            }
            else
            {
                while (decimalValue % 10 == 0)
                {
                    exponent++;
                    decimalValue /= 10;
                }
            }
            stream.WriteByte(header.GetValue());
            VariableLengthEncoding.Encode(stream, (BigInteger)decimalValue);
            VariableLengthEncoding.Encode(stream, exponent);
        }
    }
}
