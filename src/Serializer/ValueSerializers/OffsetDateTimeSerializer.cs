using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Serializer.Encoding;
using Serializer.Headers;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;
using NodaTime;
using NodaTime.Calendars;
using Serializer.Helpers;

namespace Serializer.ValueSerializers
{
    public class OffsetDateTimeSerializer : SerializerBase
    {
        public override TypeCode WireType { get; } = TypeCode.OffsetDateTime;

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var type = value.GetType();
            if (type == typeof(OffsetDateTime))
            {
                SerializeOffsetDateTime((OffsetDateTime)value, stream);
            }
            else if (type == typeof(DateTimeOffset))
            {
                SerializeDateTimeOffset((DateTimeOffset)value, stream);
            }
            else
            {
                ThrowReverseMappingError(type, TypeCode.OffsetDateTime);
            }
        }

        private void SerializeDateTimeOffset(DateTimeOffset value, Stream stream)
        {
            var precision = value.GetNeededPrecision();
            var header = OffsetDateTimeHeader.FromFields(false, value.Offset.IsNegative(), precision);
            stream.WriteByte(header.GetValue());

            var offset = Math.Abs((int)value.Offset.TotalMinutes);
            VariableLengthEncoding.Encode(offset);

            var months = (ulong)((value.Year - 1) * 12 + value.Month - 1);
            VariableLengthEncoding.Encode(stream, months);

            var days = (ulong)(value.Day - 1);
            VariableLengthEncoding.Encode(stream, days);
            if (precision == DateTimePrecision.Day) return;

            var minutes = (ulong)(value.Hour * 60 + value.Minute);
            VariableLengthEncoding.Encode(stream, minutes);
            if (precision == DateTimePrecision.Minute) return;

            var seconds = (uint)(value.Second);
            if (precision == DateTimePrecision.Second)
            {
                VariableLengthEncoding.Encode(stream, seconds);
            }
            else
            {
                var ticks = (ulong)(TimeSpan.TicksPerSecond * seconds + value.TickOfSecond());
                VariableLengthEncoding.Encode(stream, ticks);
            }
        }

        private void SerializeOffsetDateTime(OffsetDateTime value, Stream stream)
        {
            var isBC = value.Era == Era.BeforeCommon;
            var precision = value.GetNeededPrecision();
            var header = OffsetDateTimeHeader.FromFields(isBC, value.Offset.Milliseconds < 0, precision);
            stream.WriteByte(header.GetValue());

            var offset = Math.Abs(value.Offset.Milliseconds * 1000 * 60);
            VariableLengthEncoding.Encode(stream, offset);

            var months = (ulong)((value.Year - 1) * 12 + value.Month - 1);
            VariableLengthEncoding.Encode(stream, months);

            var days = (ulong)(value.Day - 1);
            VariableLengthEncoding.Encode(stream, days);
            if (precision == DateTimePrecision.Day) return;

            var minutes = (ulong)(value.Hour * 60 + value.Minute);
            VariableLengthEncoding.Encode(stream, minutes);
            if (precision == DateTimePrecision.Minute) return;

            var seconds = (uint)(value.Second);
            if (precision == DateTimePrecision.Second)
            {
                VariableLengthEncoding.Encode(stream, seconds);
            }
            else
            {
                var ticks = (ulong)(TimeSpan.TicksPerSecond * seconds + value.TickOfSecond);
                VariableLengthEncoding.Encode(stream, ticks);
            }
        }

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new OffsetDateTimeHeader(header);
            if (destinationType == typeof(DateTimeOffset))
            {
                return DeserializeDateTimeOffset(hdr, stream);
            }
            if (destinationType == typeof(OffsetDateTime))
            {
                return DeserializeOffsetDateTime(hdr, stream);
            }
            ThrowMappingError(TypeCode.OffsetDateTime, destinationType);
            return null;
        }

        private OffsetDateTime DeserializeOffsetDateTime(OffsetDateTimeHeader header, Stream stream)
        {
            var offsetMinutes = (int)VariableLengthEncoding.DecodeUint(stream);
            var months = (int)VariableLengthEncoding.DecodeUint(stream);
            var days = (int)VariableLengthEncoding.DecodeUint(stream);
            var minutes = header.Precision >= DateTimePrecision.Minute ? (int)VariableLengthEncoding.DecodeUint(stream) : 0;
            var ticks =
                header.Precision == DateTimePrecision.Second ? (long)VariableLengthEncoding.DecodeUlong(stream) * TimeSpan.TicksPerSecond :
                header.Precision == DateTimePrecision.Tick ? (long)VariableLengthEncoding.DecodeUlong(stream) : 0;

            var years = months / 12 + 1;
            months = months % 12 + 1;
            days += 1;
            var hours = minutes / 60;
            minutes %= 60;
            var seconds = ticks / TimeSpan.TicksPerSecond;
            ticks %= TimeSpan.TicksPerSecond;
            var date = new LocalDate(header.IsBC ? Era.BeforeCommon : Era.Common, years, months, days);
            var time = LocalTime.FromHourMinuteSecondTick(hours, minutes, (int)seconds, (int)ticks);
            var localDateTime = date + time;
            var offset = Offset.FromSeconds(offsetMinutes * 60);
            return new OffsetDateTime(localDateTime, offset);
        }

        private DateTimeOffset DeserializeDateTimeOffset(OffsetDateTimeHeader header, Stream stream)
        {
            if (header.IsBC) throw new InvalidOperationException("DateTime does not support BC dates.");
            var offset = (int)VariableLengthEncoding.DecodeUint(stream) * (header.IsOffsetNegative ? -1 : 1);
            var months = (int)VariableLengthEncoding.DecodeUint(stream);
            var days = (int)VariableLengthEncoding.DecodeUint(stream);
            var minutes = header.Precision >= DateTimePrecision.Minute ? (int)VariableLengthEncoding.DecodeUint(stream) : 0;
            var ticks =
                header.Precision == DateTimePrecision.Second ? (long)VariableLengthEncoding.DecodeUlong(stream) * TimeSpan.TicksPerSecond :
                header.Precision == DateTimePrecision.Tick ? (long)VariableLengthEncoding.DecodeUlong(stream) : 0;

            var years = months / 12 + 1;
            months = months % 12 + 1;
            days += 1;
            var hours = minutes / 60;
            minutes %= 60;
            var seconds = ticks / TimeSpan.TicksPerSecond;
            ticks %= TimeSpan.TicksPerSecond;

            var dateTime = new DateTime(years, months, days, hours, minutes, (int)seconds, DateTimeKind.Local);
            return new DateTimeOffset((dateTime + TimeSpan.FromTicks(ticks)), TimeSpan.FromMinutes(offset));
        }
    }
}
