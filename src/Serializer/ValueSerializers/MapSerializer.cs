﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Encoding;
using System.Linq;
using Serializer.Helpers;
using System.Reflection;

namespace Serializer.ValueSerializers
{
    class MapSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.Map;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var count = (int)VariableLengthEncoding.DecodeUint(stream);
            var mapWrapper = new MapWrapper(destinationType, count);
            context.TryUpdateDeserializationCache(WireType, mapWrapper.GetMap());
            FillMap(mapWrapper, stream, context);
            return mapWrapper.GetMap();
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var hdr = MapHeader.FromFields();
            stream.WriteByte(hdr.GetValue());
            if (MapWrapper.IsDictionary(value.GetType()))
            {
                SerializeDictionary((IDictionary)value, stream, context);
            }
            else
            {
                SerializeObject(value, stream, context);
            }
        }

        private void SerializeDictionary(IDictionary dictionary, Stream stream, SerializationContext context)
        {
            VariableLengthEncoding.Encode(stream, dictionary.Count);
            foreach(DictionaryEntry kvp in dictionary)
            {
                context.Serialize(stream, kvp.Key);
                context.Serialize(stream, kvp.Value);
            }
        }

        private void SerializeObject(object obj, Stream stream, SerializationContext context)
        {
            var properties = obj.GetType().GetTypeInfo().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            VariableLengthEncoding.Encode(stream, properties.Length);
            foreach(var prop in properties)
            {
                context.Serialize(stream, prop.Name);
                context.Serialize(stream, prop.GetGetMethod().Invoke(obj, Array.Empty<object>()));
            }
        }

        private void FillMap(MapWrapper mapWrapper, Stream stream, SerializationContext context)
        {
            for (int i = 0; i < mapWrapper.PairCount; i++)
            {
                var key = context.Deserialize(stream, mapWrapper.KeyType);
                var value = context.Deserialize(stream, mapWrapper.GetValueType(key));
                mapWrapper.Add(key, value);
            }
        }

        private class MapWrapper
        {
            public Action<object, object> Add { get; private set; }
            private object map;
            private PropertyInfo[] properties;
            private bool isDictionary;
            private Type dictionaryValueType;

            public object GetMap() => map;
            public int PairCount { get; }
            public Type KeyType { get; }

            public Type GetValueType(object key)
            {
                if (isDictionary)
                {
                    return dictionaryValueType;
                }
                var name = (string)key;
                return properties.First(x => x.Name == name).PropertyType;
            }

            public static bool IsDictionary(Type type)
            {
                type = type.GetTypeInfo().IsGenericType ? type.GetTypeInfo().GetGenericTypeDefinition() : type;
                return typeof(IDictionary).GetTypeInfo().IsAssignableFrom(type) ||
                       typeof(IDictionary<,>).GetTypeInfo().IsAssignableFrom(type);
            }

            private Type GetConcreteDictionaryType(Type type)
            {
                if (type.GetTypeInfo().IsInterface)
                {
                    if (type.GetTypeInfo().IsGenericType)
                    {
                        var genericArgs = type.GetTypeInfo().GetGenericArguments();
                        type = typeof(Dictionary<,>).MakeGenericType(genericArgs);
                    }
                    else
                    {
                        type = typeof(Dictionary<object, object>);
                    }
                }
                return type;
            }

            public MapWrapper(Type type, int pairCount)
            {
                PairCount = pairCount;
                if (IsDictionary(type))
                {
                    isDictionary = true;
                    var mapType = GetConcreteDictionaryType(type);
                    var instance = (IDictionary)Activator.CreateInstance(mapType);
                    var genericArgs = mapType.GetTypeInfo().GetGenericArguments();
                    KeyType = genericArgs[0];
                    dictionaryValueType = genericArgs[1];
                    map = instance;
                    Add = instance.Add;
                }
                else if (!type.GetTypeInfo().IsInterface)
                {
                    isDictionary = false;
                    map = Activator.CreateInstance(type);
                    KeyType = typeof(string);
                    properties = type.GetTypeInfo().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
                    Add = (key, value) =>
                    {
                        var propertyName = (string)key;
                        var property = properties.First(x => x.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
                        value = UniCast.ChangeType(value, property.PropertyType);
                        property.GetSetMethod().Invoke(map, new[] { value });
                    };
                }
                else
                {
                    throw new InvalidOperationException($"Could not create an interface implementation for type {type}");
                }
            }
        }
    }
}
