﻿using System;
using System.IO;
using Serializer.Headers;
using Serializer.Extensions;
using Serializer.Encoding;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Helpers;
using System.Numerics;
using System.Reflection;

namespace Serializer.ValueSerializers
{
    public class BinaryNumberSerializer : SerializerBase
    {
        public override TypeCode WireType { get; } = TypeCode.BinaryNumber;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new BinaryNumberHeader(header);
            AssertValidTypeConversion(hdr.NumberType, destinationType);
            object value = null;
            switch (hdr.NumberType)
            {
                case BinaryNumberType.Float32:
                    value = ReadFloat32(stream);
                    break;
                case BinaryNumberType.Float64:
                    value = ReadFloat64(stream);
                    break;
                case BinaryNumberType.NegativeVLEInteger:
                    value = ReadNegativeInteger(stream);
                    break;
                case BinaryNumberType.PositiveVLEInteger:
                    value = ReadPositiveInteger(stream);
                    break;
            }
            return value.ConvertTo(destinationType);
        }

        private void AssertValidTypeConversion(BinaryNumberType source, Type destination)
        {
            bool isValid = false;
            switch (source)
            {
                case BinaryNumberType.Float32:
                    isValid = destination.IsOneOf(typeof(float), typeof(double));
                    break;
                case BinaryNumberType.Float64:
                    isValid = destination == typeof(double);
                    break;
                case BinaryNumberType.NegativeVLEInteger:
                    isValid = destination.IsOneOf(typeof(sbyte), typeof(short), typeof(int), typeof(long));
                    break;
                case BinaryNumberType.PositiveVLEInteger:
                    isValid = destination.IsOneOf(typeof(byte), typeof(sbyte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong));
                    break;
            }
            if (!isValid) ThrowMappingError(source, destination);
        }

        private object ReadFloat32(Stream stream)
        {
            var bytes = stream.ReadVector(4);
            bytes.ReverseIfLittleEndian();
            return BitConverter.ToSingle(bytes.UnderlyingBuffer, 0);
        }

        private object ReadFloat64(Stream stream)
        {
            var bytes = stream.ReadVector(8);
            bytes.ReverseIfLittleEndian();
            return BitConverter.ToDouble(bytes.UnderlyingBuffer, 0);
        }

        private ulong ReadPositiveInteger(Stream stream)
        {
            return VariableLengthEncoding.DecodeUlong(stream);
        }

        private long ReadNegativeInteger(Stream stream)
        {
            var value = VariableLengthEncoding.DecodeBigInteger(stream);
            return (long)(-value);
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var type = value.GetType();
            BinaryNumberHeader header;
            ByteVector vector;
            if (type == typeof (float))
            {
                header = BinaryNumberHeader.FromFields(BinaryNumberType.Float32);
                vector = new ByteVector(BitConverter.GetBytes((float) value));
                vector.ReverseIfLittleEndian();
            }
            else if (type == typeof(double))
            {
                header = BinaryNumberHeader.FromFields(BinaryNumberType.Float64);
                vector = new ByteVector(BitConverter.GetBytes((double)value));
                vector.ReverseIfLittleEndian();
            }
            else if (type.IsOneOf(typeof(byte), typeof(ushort), typeof(uint), typeof(ulong)))
            {
                header = BinaryNumberHeader.FromFields(BinaryNumberType.PositiveVLEInteger);
                vector = VariableLengthEncoding.Encode(Convert.ToUInt64(value));
            }
            else if (type.IsOneOf(typeof(sbyte), typeof(short), typeof(int), typeof(long)))
            {
                var tmpValue = Convert.ToInt64(value);
                header = BinaryNumberHeader.FromFields(tmpValue < 0 ? BinaryNumberType.NegativeVLEInteger : BinaryNumberType.PositiveVLEInteger);
                vector = VariableLengthEncoding.Encode(tmpValue);
            }
            else if (type == typeof(BigInteger))
            {
                var tmpValue = (BigInteger)value;
                header = BinaryNumberHeader.FromFields(tmpValue.IsNegative() ? BinaryNumberType.NegativeVLEInteger : BinaryNumberType.PositiveVLEInteger);
                vector = VariableLengthEncoding.Encode(tmpValue);
            }
            else
            {
                throw new ArgumentException($"This serializer cannot serialize type {type.Name}.");
            }
            stream.WriteByte(header.GetValue());
            stream.WriteVector(vector);
        }
    }
}
