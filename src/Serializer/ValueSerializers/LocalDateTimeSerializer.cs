using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Serializer.Encoding;
using Serializer.Headers;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;
using NodaTime;
using NodaTime.Calendars;
using Serializer.Helpers;

namespace Serializer.ValueSerializers
{
    public class LocalDateTimeSerializer : SerializerBase
    {
        public override TypeCode WireType { get; } = TypeCode.LocalDateTime;

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var type = value.GetType();
            if (type == typeof(LocalDateTime))
            {
                SerializeLocalDateTime((LocalDateTime)value, stream);
            }
            else if (type == typeof(DateTime))
            {
                SerializeDateTime((DateTime)value, stream);
            }
            else
            {
                ThrowReverseMappingError(type, TypeCode.LocalDateTime);
            }
        }

        private void SerializeDateTime(DateTime value, Stream stream)
        {
            var precision = value.GetNeededPrecision();
            var header = LocalDateTimeHeader.FromFields(false, precision);
            stream.WriteByte(header.GetValue());

            var months = (ulong)((value.Year - 1) * 12 + value.Month - 1);
            VariableLengthEncoding.Encode(stream, months);

            var days = (ulong)(value.Day - 1);
            VariableLengthEncoding.Encode(stream, days);
            if (precision == DateTimePrecision.Day) return;

            var minutes = (ulong)(value.Hour * 60 + value.Minute);
            VariableLengthEncoding.Encode(stream, minutes);
            if (precision == DateTimePrecision.Minute) return;

            var seconds = (uint)(value.Second);
            if (precision == DateTimePrecision.Second)
            {
                VariableLengthEncoding.Encode(stream, seconds);
            }
            else
            {
                var ticks = (ulong)(TimeSpan.TicksPerSecond * seconds + value.TickOfSecond());
                VariableLengthEncoding.Encode(stream, ticks);
            }
        }

        private void SerializeLocalDateTime(LocalDateTime value, Stream stream)
        {
            var isBC = value.Era == Era.BeforeCommon;
            var precision = value.GetNeededPrecision();
            var header = LocalDateTimeHeader.FromFields(isBC, precision);
            stream.WriteByte(header.GetValue());

            var months = (ulong)((value.Year - 1) * 12 + value.Month - 1);
            VariableLengthEncoding.Encode(stream, months);

            var days = (ulong)(value.Day - 1);
            VariableLengthEncoding.Encode(stream, days);
            if (precision == DateTimePrecision.Day) return;

            var minutes = (ulong)(value.Hour * 60 + value.Minute);
            VariableLengthEncoding.Encode(stream, minutes);
            if (precision == DateTimePrecision.Minute) return;

            var seconds = (uint)(value.Second);
            if (precision == DateTimePrecision.Second)
            {
                VariableLengthEncoding.Encode(stream, seconds);
            }
            else
            {
                var ticks = (ulong)(TimeSpan.TicksPerSecond * seconds + value.TickOfSecond);
                VariableLengthEncoding.Encode(stream, ticks);
            }
        }

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new LocalDateTimeHeader(header);
            if (destinationType == typeof(DateTime))
            {
                return DeserializeDateTime(hdr, stream);
            }
            if (destinationType == typeof(LocalDateTime))
            {
                return DeserializeLocalDate(hdr, stream);
            }
            ThrowMappingError(TypeCode.LocalDateTime, destinationType);
            return null;
        }

        private LocalDateTime DeserializeLocalDate(LocalDateTimeHeader header, Stream stream)
        {
            var months = (int)VariableLengthEncoding.DecodeUint(stream);
            var days = (int)VariableLengthEncoding.DecodeUint(stream);
            var minutes = header.Precision >= DateTimePrecision.Minute ? (int)VariableLengthEncoding.DecodeUint(stream) : 0;
            var ticks =
                header.Precision == DateTimePrecision.Second ? (long)VariableLengthEncoding.DecodeUlong(stream) * TimeSpan.TicksPerSecond :
                header.Precision == DateTimePrecision.Tick ? (long)VariableLengthEncoding.DecodeUlong(stream) : 0;

            var years = months / 12 + 1;
            months = months % 12 + 1;
            days += 1;
            var hours = minutes / 60;
            minutes %= 60;
            var seconds = ticks / TimeSpan.TicksPerSecond;
            ticks %= TimeSpan.TicksPerSecond;
            var date = new LocalDate(header.IsBC ? Era.BeforeCommon : Era.Common, years, months, days);
            var time = LocalTime.FromHourMinuteSecondTick(hours, minutes, (int)seconds, (int)ticks);
            return date + time;
        }

        private DateTime DeserializeDateTime(LocalDateTimeHeader header, Stream stream)
        {
            if (header.IsBC) throw new InvalidOperationException("DateTime does not support BC dates.");
            var months = (int)VariableLengthEncoding.DecodeUint(stream);
            var days = (int)VariableLengthEncoding.DecodeUint(stream);
            int minutes = header.Precision >= DateTimePrecision.Minute ? (int)VariableLengthEncoding.DecodeUint(stream) : 0;
            var ticks =
                header.Precision == DateTimePrecision.Second ? (long)VariableLengthEncoding.DecodeUlong(stream) * TimeSpan.TicksPerSecond :
                header.Precision == DateTimePrecision.Tick ? (long)VariableLengthEncoding.DecodeUlong(stream) : 0;

            var years = months / 12 + 1;
            months = months % 12 + 1;
            days += 1;
            var hours = minutes / 60;
            minutes %= 60;
            var seconds = ticks / TimeSpan.TicksPerSecond;
            ticks %= TimeSpan.TicksPerSecond;

            var dateTime = new DateTime(years, months, days, hours, minutes, (int)seconds, DateTimeKind.Local);
            return dateTime + TimeSpan.FromTicks(ticks);
        }
    }
}
