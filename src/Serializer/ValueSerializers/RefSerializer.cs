﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;
using Serializer.Encoding;

namespace Serializer.ValueSerializers
{
    class RefSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.Ref;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new RefHeader(header);
            if (hdr.ReferenceType == ReferenceType.ByValue)
            {
                throw new NotSupportedException("ByValue references are not implemented yet.");
            }
            int index = (int)VariableLengthEncoding.DecodeUint(stream);
            return context.GetDeserializationCacheEntry(index);
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var index = (int)value;
            var hdr = RefHeader.FromFields(ReferenceType.ByReference);
            stream.WriteByte(hdr.GetValue());
            VariableLengthEncoding.Encode(stream, index);
        }
    }
}
