using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Serializer.Encoding;
using Serializer.Headers;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;
using NodaTime;
using Serializer.Helpers;

namespace Serializer.ValueSerializers
{
    public class TimeSpanSerializer : SerializerBase
    {
        public override TypeCode WireType { get; } = TypeCode.TimeSpan;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            var hdr = new TimeSpanHeader(header);
            if (destinationType == typeof(TimeSpan))
            {
                return ReadTimeSpan(ReadRawData(stream, hdr.Precision), hdr.IsNegative);
            }
            if (destinationType == typeof(Period))
            {
                return ReadPeriod(ReadRawData(stream, hdr.Precision), hdr.IsNegative);
            }
            ThrowMappingError(TypeCode.TimeSpan, destinationType);
            return null;
        }

        private Period ReadPeriod(IntermediateData data, bool isNegative)
        {
            var sign = isNegative ? -1 : 1;
            var builder = new PeriodBuilder();
            builder.Years = data.Years * sign;
            builder.Months = data.Months * sign;
            builder.Days = data.Days * sign;
            builder.Hours = data.Hours * sign;
            builder.Minutes = data.Minutes * sign;
            builder.Seconds = data.Seconds * sign;
            builder.Ticks = data.Ticks * sign;
            return builder.Build();
        }

        private TimeSpan ReadTimeSpan(IntermediateData data, bool isNegative)
        {
            if (data.Years != 0 || data.Months != 0)
            {
                throw new InvalidOperationException("TimeSpan type does not support time units greater than days.");
            }
            var sign = isNegative ? -1 : 1;
            var ts = new TimeSpan(
                data.Days * sign, 
                data.Hours * sign, 
                data.Minutes * sign, 
                data.Seconds * sign);
            ts += TimeSpan.FromTicks(data.Ticks * sign);
            return ts;
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            var type = value.GetType();
            if (type == typeof(TimeSpan))
            {
                WriteTimeSpan(stream, (TimeSpan)value);
                return;
            }
            if (type == typeof(Period))
            {
                WritePeriod(stream, (Period)value);
                return;
            }
            ThrowReverseMappingError(type, TypeCode.TimeSpan);
        }

        private void WritePeriod(Stream stream, Period value)
        {
            value = Normalize(value);
            var precision = GetRequiredPrecision(value);
            var sign = IsNegative(value);
            var header = TimeSpanHeader.FromFields(sign, precision);

            stream.WriteByte(header.GetValue());

            VariableLengthEncoding.Encode(stream, Math.Abs(value.Months));
            VariableLengthEncoding.Encode(stream, Math.Abs(value.Days));
            if (precision == DateTimePrecision.Day) return;
            VariableLengthEncoding.Encode(stream, Math.Abs(value.Minutes));
            if (precision == DateTimePrecision.Minute) return;
            if (precision == DateTimePrecision.Second)
            {
                VariableLengthEncoding.Encode(stream, Math.Abs(value.Ticks / TimeSpan.TicksPerSecond));
            }
            else
            {
                VariableLengthEncoding.Encode(stream, Math.Abs(value.Ticks));
            }
        }

        private DateTimePrecision GetRequiredPrecision(Period value)
        {
            var precision = DateTimePrecision.Day;
            if (value.Minutes != 0) precision = DateTimePrecision.Minute;
            if (value.Ticks != 0)
            {
                if (value.Ticks % TimeSpan.TicksPerSecond == 0) precision = DateTimePrecision.Second;
                else precision = DateTimePrecision.Tick;
            }
            return precision;
        }

        private bool IsNegative(Period period)
        {
            if (period.Months < 0) return true;
            if (period.Days < 0) return true;
            if (period.Minutes < 0) return true;
            if (period.Ticks < 0) return true;
            return false;
        }

        private Period Normalize(Period period)
        {
            var builder = new PeriodBuilder();
            builder.Months = period.Years * 12 + period.Months;
            builder.Days = period.Days + period.Weeks * 7;
            builder.Minutes = period.Hours * 60 + period.Minutes;
            builder.Ticks =
                period.Seconds * TimeSpan.TicksPerSecond +
                period.Milliseconds * TimeSpan.TicksPerMillisecond +
                period.Ticks +
                period.Nanoseconds / 100;
            var allPositive = builder.Months >= 0 && builder.Days >= 0 && builder.Minutes >= 0 && builder.Ticks >= 0;
            var allNegative = builder.Months <= 0 && builder.Days <= 0 && builder.Minutes <= 0 && builder.Ticks <= 0;
            if (!(allPositive || allNegative))
            {
                throw new InvalidOperationException("Could not normalize the period: cannot determine the sign.");
            }
            return builder.Build();
        }

        private void WriteTimeSpan(Stream stream, TimeSpan value)
        {
            var builder = new PeriodBuilder();
            builder.Days = value.Days;
            builder.Hours = value.Hours;
            builder.Minutes = value.Minutes;
            builder.Seconds = value.Seconds;
            builder.Ticks = value.TickOfSecond();
            WritePeriod(stream, builder.Build());
        }

        private struct IntermediateData
        {
            public int Years;
            public int Months;
            public int Days;
            public int Hours;
            public int Minutes;
            public int Seconds;
            public long Ticks;
        }

        private static IntermediateData ReadRawData(Stream stream, DateTimePrecision precision)
        {
            var id = new IntermediateData();
            id.Months = (int)VariableLengthEncoding.DecodeUint(stream);
            id.Days = (int)VariableLengthEncoding.DecodeUint(stream);
            id.Minutes = precision < DateTimePrecision.Minute ? 0 : (int)VariableLengthEncoding.DecodeUint(stream);
            id.Ticks =
                precision == DateTimePrecision.Second ? (int)VariableLengthEncoding.DecodeUint(stream) * TimeSpan.TicksPerSecond :
                precision == DateTimePrecision.Tick ? (int)VariableLengthEncoding.DecodeUint(stream) :
                0;
            id.Years = id.Months / 12;
            id.Months %= 12;
            id.Hours = id.Minutes / 60;
            id.Minutes %= 60;
            id.Seconds = (int)(id.Ticks / TimeSpan.TicksPerSecond);
            id.Ticks %= TimeSpan.TicksPerSecond;
            return id;
        }
    }
}
