﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serializer.Headers;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.ValueSerializers
{
    class NullSerializer : SerializerBase
    {
        public override TypeCode WireType => TypeCode.Null;

        public override object Deserialize(byte header, Stream stream, Type destinationType, SerializationContext context)
        {
            if (destinationType == typeof(DBNull))
            {
                return DBNull.Value;
            }
            else return null;
        }

        public override void Serialize(object value, Stream stream, SerializationContext context)
        {
            if (value != null && value != DBNull.Value)
            {
                throw new InvalidOperationException("Unsupported type");
            }
            var hdr = new NullHeader();
            stream.WriteByte(hdr.GetValue());
        }
    }
}
