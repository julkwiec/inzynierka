using System;
using System.IO;
using System.Numerics;
using Serializer.Extensions;
using Serializer.Helpers;

namespace Serializer.Encoding
{
    static class VariableLengthEncoding
    {
        public static ByteVector Encode(byte value) => Encode((ulong)value);
        public static ByteVector Encode(ushort value) => Encode((ulong)value);
        public static ByteVector Encode(uint value) => Encode((ulong)value);
        public static ByteVector Encode(ulong value)
        {
            var totalSegmentCount = GetVLESegmentCount(value);
            var vector = new ByteVector(totalSegmentCount);
            for (int i = 0; i < totalSegmentCount; i++)
            {
                var segment = GetSegment(value, i, totalSegmentCount);
                vector.Add(segment);
            }
            return vector;
        }

        public static ByteVector Encode(BigInteger value) => Encode(value.ToBigEndianByteVector());
        public static ByteVector Encode(ByteVector bigEndianBytes)
        {
            var totalSegmentCount = GetVLESegmentCount(bigEndianBytes);
            var vector = new ByteVector(totalSegmentCount);
            for (int i = 0; i < totalSegmentCount; i++)
            {
                var segment = GetSegment(bigEndianBytes, i, totalSegmentCount);
                vector.Add(segment);
            }
            return vector;
        }

        public static void Encode(Stream stream, byte value) => Encode(stream, (ulong)value);
        public static void Encode(Stream stream, ushort value) => Encode(stream, (ulong)value);
        public static void Encode(Stream stream, uint value) => Encode(stream, (ulong)value);
        public static void Encode(Stream stream, ulong value)
        {
            var totalSegmentCount = GetVLESegmentCount(value);
            for (int i = 0; i < totalSegmentCount; i++)
            {
                var segment = GetSegment(value, i, totalSegmentCount);
                stream.WriteByte(segment);
            }
        }

        public static void Encode(Stream stream, BigInteger value) => Encode(stream, value.ToBigEndianByteVector());
        public static void Encode(Stream stream, ByteVector bigEndianBytes)
        {
            var totalSegmentCount = GetVLESegmentCount(bigEndianBytes);
            for (int i = 0; i < totalSegmentCount; i++)
            {
                var segment = GetSegment(bigEndianBytes, i, totalSegmentCount);
                stream.WriteByte(segment);
            }
        }

        public static byte DecodeByte(Stream stream) => (byte)DecodeUlong(stream);
        public static ushort DecodeUshort(Stream stream) => (ushort)DecodeUlong(stream);
        public static uint DecodeUint(Stream stream) => (uint)DecodeUlong(stream);
        public static ulong DecodeUlong(Stream stream)
        {
            ulong value = 0;
            while (true)
            {
                var segment = stream.ReadByteOrFail();
                value = (value << 7) | (segment.WriteBit(0, false));
                if (IsLastByte(segment)) break;
            }
            return value;
        }

        public static ByteVector DecodeBigEndianBytes(Stream stream) => DecodeBigInteger(stream).ToBigEndianByteVector();
        public static BigInteger DecodeBigInteger(Stream stream)
        {
            BigInteger value = 0;
            while (true)
            {
                var segment = stream.ReadByteOrFail();
                value = (value << 7) | (segment.WriteBit(0, false));
                if (IsLastByte(segment)) break;
            }
            return value;
        }

        public static byte DecodeByte(ByteVector bytes) => (byte)DecodeUlong(bytes);
        public static ushort DecodeUshort(ByteVector bytes) => (ushort)DecodeUlong(bytes);
        public static uint DecodeUint(ByteVector bytes) => (uint)DecodeUlong(bytes);
        public static ulong DecodeUlong(ByteVector bytes)
        {
            ulong value = 0;
            int byteIndex = 0;
            while (true)
            {
                var segment = bytes[byteIndex];
                byteIndex++;
                value = (value << 7) | (segment.WriteBit(0, false));
                if (IsLastByte(segment)) break;
            }
            return value;
        }

        public static ByteVector DecodeBigEndianBytes(ByteVector bytes) => DecodeBigInteger(bytes).ToBigEndianByteVector();
        public static BigInteger DecodeBigInteger(ByteVector bytes)
        {
            BigInteger value = 0;
            int byteIndex = 0;
            while (true)
            {
                var segment = bytes[byteIndex];
                byteIndex++;
                value = (value << 7) | (segment.WriteBit(0, false));
                if (IsLastByte(segment)) break;
            }
            return value;
        }

        private static byte GetSegment(ulong value, int segmentIndex, int segmentCount)
        {
            int shift = 7 * (segmentCount - segmentIndex - 1);
            bool isNotLast = segmentIndex < segmentCount - 1;
            byte segment = unchecked((byte)(value >> shift));
            return segment.WriteBit(0, isNotLast);
        }

        private static byte GetSegment(ByteVector bigEndianBytes, int segmentIndex, int segmentCount)
        {
            var bitVector = new BitVector(bigEndianBytes);
            int totalBits = bitVector.GetSignificantBitCount();
            int firstSignificantBitIndex = bigEndianBytes.Count * 8 - totalBits;
            int firstSegmentLength = (totalBits - 1) % 7 + 1;

            int segmentStartIndex =
                firstSignificantBitIndex +
                (segmentIndex == 0 ? 0 : firstSegmentLength) +
                7 * (Math.Max(0, segmentIndex - 1));
            int segmentLength = segmentIndex == 0 ? firstSegmentLength : 7;

            var segment = bitVector.ReadSegmentAt(segmentStartIndex, segmentLength);
            bool isNotLast = segmentIndex < segmentCount - 1;
            segment = segment.WriteBit(0, isNotLast);
            return segment;
        }

        private static int GetVLESegmentCount(ulong value)
        {
            return GetSignificantBitCount(value).DivideRoundUp(7);
        }

        private static int GetVLESegmentCount(ByteVector bigEndianBytes)
        {
            return GetSignificantBitCount(bigEndianBytes).DivideRoundUp(7);
        }

        public static bool IsLastByte(byte value)
        {
            return !value.ReadBit(0);
        }

        public static bool ValidateByte(byte value, bool isFirst)
        {
            return isFirst || value == 0x80;
        }

        public static bool ValidateVector(ByteVector vector)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                if (!ValidateByte(vector[i], i == 0)) return false;
            }
            return true;
        }

        public static void ValidateVectorOrFail(ByteVector vector)
        {
            if (!ValidateVector(vector)) throw new ArgumentException();
        }

        public static void ValidateByteOrFail(byte value, bool isFirst)
        {
            if (!ValidateByte(value, isFirst)) throw new ArgumentException();
        }

        private static int GetFirstSegmentLength(int significantBitCount)
        {
            var value = significantBitCount % 7;
            return value == 0 ? 7 : value;
        }

        private static int GetSignificantBitCount(ulong value)
        {
            int bits = 0;
            while(value > 0)
            {
                value >>= 1;
                bits++;
            }
            return Math.Max(bits, 1);
        }

        private static int GetSignificantBitCount(ByteVector vector)
        {
            int significantBytes = vector.Length;
            for(int i = 0; i < vector.Length && vector[i] == 0; i++)
            {
                significantBytes--;
            }
            significantBytes = Math.Max(significantBytes, 1);
            int msbSignificantBits = 0;
            byte msb = vector[vector.Length - significantBytes];
            while(msb > 0)
            {
                msb >>= 1;
                msbSignificantBits++;
            }
            var sbc = (significantBytes - 1) * 8 + msbSignificantBits;
            return Math.Max(sbc, 1);
        }
    }
}
