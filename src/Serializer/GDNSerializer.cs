﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Serializer
{
    public static class GDNSerializer
    {
        public static T Deserialize<T>(Stream stream) => (T)Deserialize(stream, typeof(T));
        public static object Deserialize(Stream stream, Type targetType)
        {
            return new SerializationContext().Deserialize(stream, targetType);
        }

        public static T Deserialize<T>(byte[] data) => (T)Deserialize(data, typeof(T));
        public static object Deserialize(byte[] data, Type targetType)
        {
            using (var stream = new MemoryStream(data))
            {
                return Deserialize(stream, targetType);
            }
        }

        public static void Serialize(Stream stream, object value)
        {
            new SerializationContext().Serialize(stream, value);
        }

        public static byte[] Serialize(object value)
        {
            byte[] result;
            using (var stream = new MemoryStream())
            {
                Serialize(stream, value);
                result = stream.ToArray();
            }
            return result;
        }
    }
}
