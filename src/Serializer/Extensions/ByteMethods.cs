using System;
using System.IO;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Extensions
{
    public static class ByteMethods
    {
        public static byte AsHeader(this TypeCode typeCode)
        {
            return (byte)((byte)typeCode << 4);
        }

        /// <summary>
        /// MSB is 0, LSB is 7
        /// </summary>
        /// <param name="value"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool ReadBit(this byte value, int number)
        {
            if (number < 0 || number > 7) throw new ArgumentOutOfRangeException($"Bit index must be between 0 and 7, but was {number}");
            return (byte)((byte) (1 << (7 - number)) & value) > 0;
        }

        public static byte WriteBit(this byte @byte, int number, bool value)
        {
            if (value)
            {
                @byte |= (byte) (0x80 >> number);
            }
            else
            {
                @byte &= (byte) ~(0x80 >> number);
            }
            return @byte;
        }

        public static byte ReadNumber(this byte value, int start, int count)
        {
            if (start < 0 || start > 7) throw new ArgumentOutOfRangeException($"Bit index must be between 0 and 7, but was {start}");
            if (count < 1) throw new ArgumentOutOfRangeException($"Bit count must be equal to or greater than 1, but was {count}");
            var end = start + count - 1;
            if (start + count > 8) throw new ArgumentOutOfRangeException($"Cannot access bit beyond 7, tried {end} with start = {start} and count = {count}");

            int ret = 0;
            int offset = 0;
            for (int i = end; i >= start; i--)
            {
                if (value.ReadBit(i))
                {
                    ret |= 1 << offset;
                }
                offset++;
            }

            return (byte)ret;
        }

        public static byte WriteNumber(this byte @byte, int start, int count, byte value)
        {
            if (start < 0 || start > 7) throw new ArgumentException("Start index exceeds byte range");
            if (count < 1 || start + count > 8) throw new ArgumentException();
            if (value >> count != 0) throw new ArgumentException();

            var mask = (byte)~(0xFF << count);
            var shift = 8 - start - count;
            var shiftedMask = (byte)~(mask << shift);
            var shiftedValue = value << shift;
            return (byte)((@byte & shiftedMask) | shiftedValue);
        }

        public static byte WriteNumber(this byte @byte, int start, int count, int value)
        {
            if (value < 0 || value > 255) throw new ArgumentOutOfRangeException();
            return WriteNumber(@byte, start, count, (byte)value);
        }

        public static byte[] ReadBytes(this Stream stream, int count)
        {
            var ret = new byte[count];
            var read = stream.Read(ret, 0, count);
            if (read == count)
            {
                return ret;
            }
            throw new InvalidOperationException($"Unexpected enf od stream. Tried to read {count} bytes, got {read} instead.");
        }

        public static void WriteBytes(this Stream stream, byte[] bytes)
        {
            stream.Write(bytes, 0, bytes.Length);
        }

        public static bool ReadBit(this byte[] bytes, int index)
        {
            int byteIndex = index/8;
            byte bitMask = (byte)(0x80 >> (index%8));
            return (bytes[byteIndex] & bitMask) > 0;
        }

        public static void WriteBit(this byte[] bytes, int index, bool value)
        {
            var byteIndex = index/8;
            var bitIndex = index%8;
            bytes[byteIndex] = bytes[byteIndex].WriteBit(bitIndex, value);
        }

        public static byte ReadByte(this ArraySegment<byte> bytes, int index)
        {
            return bytes.Array[bytes.Offset + index];
        }

        public static bool ReadBit(this ArraySegment<byte> bytes, int index)
        {
            return bytes.Array.ReadBit(bytes.Offset + index);
        }

        public static ArraySegment<byte> SkipLeadingZeros(this byte[] array)
            => SkipLeadingZeros(new ArraySegment<byte>(array));

        public static ArraySegment<byte> SkipLeadingZeros(this ArraySegment<byte> array)
        {
            int i = array.Offset;
            int end = array.Offset + array.Count;
            for (; i < end; i++)
            {
                if (array.Array[i] != 0) break;
            }
            i = Math.Min(end - 1, i);
            return new ArraySegment<byte>(array.Array, i, end - i);
        }

        public static byte[] ToSystemByteOrder(this byte[] bytes)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
            return bytes;
        }

        public static byte[] ToNetworkByteOrder(this byte[] bytes)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
            return bytes;
        }
    }
}
