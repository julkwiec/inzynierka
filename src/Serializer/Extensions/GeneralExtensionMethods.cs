using Serializer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace Serializer.Extensions
{
    public static class GeneralExtensionMethods
    {
        public static TRet Map<TArg, TRet>(this TArg arg, Func<TArg, TRet> transform)
        {
            if (arg == null) return default(TRet);
            return transform(arg);
        }

        public static TRet Map<TArg, TRet>(this TArg arg, Func<TArg, TRet> transform, Func<TRet> nullFunc)
        {
            if (arg == null) return nullFunc();
            return transform(arg);
        }

        public static T Do<T>(this T item, Action<T> action)
        {
            action(item);
            return item;
        }

        public static object ConvertTo(this object item, Type destinationType)
        {
            return UniCast.ChangeType(item, destinationType);
        }

        public static T ConvertTo<T>(this object item)
        {
            try
            {
                return (T)UniCast.ChangeType(item, typeof(T));
            }
            catch (Exception ex)
            {
                throw new InvalidCastException(
                    $@"""{item.GetType().FullName}"" (value: {item}) cannot be cast to ""{typeof(T).FullName}""",
                    ex);
            }
        }

        public static T ConvertTo<T>(this object item, T defaultValue)
        {
            try
            {
                return item.ConvertTo<T>();
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        public static T ConvertToFun<T>(this object item, Func<T> defaultValueGenerator)
        {
            try
            {
                return item.ConvertTo<T>();
            }
            catch (Exception)
            {
                return defaultValueGenerator();
            }
        }

        public static T CastTo<T>(this object item)
        {
            return (T)item;
        }

        public static bool IsOneOf<T>(this T item, IEnumerable<T> items)
        {
            return items.Contains(item);
        }

        public static bool IsOneOf<T>(this T item, params T[] items)
        {
            return item.IsOneOf(items.AsEnumerable());
        }

        public static void ThrowPreservingStackTrace(this Exception ex)
        {
            ExceptionDispatchInfo.Capture(ex).Throw();
        }

        public static bool IsDefined<T>(this T value) where T : struct
        {
            return Enum.IsDefined(typeof(T), value);
        }
    }
}