using Serializer.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Serializer.Extensions
{
    public static class StreamExtensions
    {
        public static void Write(this Stream stream, ArraySegment<byte> data)
        {
            stream.Write(data.Array, data.Offset, data.Count);
        }

        public static byte ReadByteOrFail(this Stream stream)
        {
            var value = stream.ReadByte();
            if (value < 0) throw new InvalidOperationException("Unexpected end of stream.");
            return (byte)value;
        }

        public static ByteVector ReadVector(this Stream stream, int length)
        {
            var vector = new ByteVector(length);
            if (stream.Read(vector.UnderlyingBuffer, 0, length) != length)
            {
                throw new InvalidOperationException("Unexpected end of stream.");
            }
            vector.SetLength(length);
            return vector;
        }

        public static void WriteVector(this Stream stream, ByteVector vector)
        {
            stream.Write(vector.UnderlyingBuffer, 0, vector.Length);
        }
    }
}
