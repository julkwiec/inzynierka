using Serializer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace Serializer.Extensions
{
    static class MathExtensionMethods
    {
        public static int DivideRoundUp(this int value, int divider)
        {
            return (value + divider - 1)/divider;
        }

        private static readonly DateTime MinimalNonNegativeDateTime = new DateTime(1,1,1);

        public static bool IsNegative(this sbyte value) => value < 0;
        public static bool IsNegative(this short value) => value < 0;
        public static bool IsNegative(this int value) => value < 0;
        public static bool IsNegative(this long value) => value < 0;
        public static bool IsNegative(this BigInteger value) => value.Sign < 0;
        public static bool IsNegative(this float value) => value < 0;
        public static bool IsNegative(this double value) => value < 0;
        public static bool IsNegative(this DateTime value) => value < MinimalNonNegativeDateTime;
        public static bool IsNegative(this DateTimeOffset value) => value < MinimalNonNegativeDateTime;
        public static bool IsNegative(this TimeSpan value) => value < TimeSpan.Zero;

        public static byte ShiftLeftSafe(this byte value, int shift)
        {
            var mask = (byte.MaxValue >> shift) ^ byte.MaxValue;
            if ((value & mask) != 0)
            {
                throw new InvalidOperationException();
            }
            return (byte)(value << shift);
        }

        public static ushort ShiftLeftSafe(this ushort value, int shift)
        {
            var mask = (ushort.MaxValue >> shift) ^ ushort.MaxValue;
            if ((value & mask) != 0)
            {
                throw new InvalidOperationException();
            }
            return (ushort)(value << shift);
        }

        public static uint ShiftLeftSafe(this uint value, int shift)
        {
            var mask = (uint.MaxValue >> shift) ^ uint.MaxValue;
            if ((value & mask) != 0)
            {
                throw new InvalidOperationException();
            }
            return (uint)(value << shift);
        }

        public static ulong ShiftLeftSafe(this ulong value, int shift)
        {
            var mask = (ulong.MaxValue >> shift) ^ ulong.MaxValue;
            if ((value & mask) != 0)
            {
                throw new InvalidOperationException();
            }
            return (ulong)(value << shift);
        }

        public static BigInteger ShiftLeftSafe(this BigInteger value, int shift)
        {
            return value << shift;
        }

        public static sbyte WithSign(this byte value, bool isNegative)
        {
            var ret = (sbyte)value;
            return (sbyte)(isNegative ? -ret : ret);
        }

        public static short WithSign(this ushort value, bool isNegative)
        {
            var ret = (short)value;
            return (short)(isNegative ? -ret : ret);
        }

        public static int WithSign(this uint value, bool isNegative)
        {
            var ret = (int)value;
            return (int)(isNegative ? -ret : ret);
        }

        public static long WithSign(this ulong value, bool isNegative)
        {
            var ret = (long)value;
            return (long)(isNegative ? -ret : ret);
        }

        public static ByteVector ToBigEndianByteVector(this BigInteger value)
        {
            if (value.Sign < 0)
            {
                value = -value;
            }
            var array = value.ToByteArray();
            var vector = new ByteVector(array);
            vector.ReverseIfLittleEndian();
            return vector;
        }
    }
}
