﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Serializer.Extensions
{
    static class TypeExtensions
    {
        public static Type Unwrap(this Type type)
        {
            if (type == null) return null;
            if (type.GetTypeInfo().IsGenericType && type.GetTypeInfo().GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                type = type.GetTypeInfo().GetGenericArguments()[0];
            }
            if (type.GetTypeInfo().IsEnum)
            {
                type = type.GetTypeInfo().GetEnumUnderlyingType();
            }
            return type;
        }
    }
}
