using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Serializer.Extensions
{
    public static class StringExtensionMethods
    {
        private static readonly System.Text.Encoding Utf8 = new UTF8Encoding(false, false);

        public static string DecodeUTF8(this byte[] data)
        {
            return Utf8.GetString(data);
        }

        public static string DecodeUTF8(this ArraySegment<byte> data)
        {
            return Utf8.GetString(data.Array, data.Offset, data.Count);
        }

        public static byte[] EncodeUTF8(this string str)
        {
            return Utf8.GetBytes(str);
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return String.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhitespace(this string str)
        {
            return String.IsNullOrWhiteSpace(str);
        }

        public static string IfEmptyThenNull(this string str)
        {
            return String.IsNullOrEmpty(str) ? null : str;
        }

        public static string IfEmptyOrWhitespaceThenNull(this string str)
        {
            return String.IsNullOrWhiteSpace(str) ? null : str;
        }

        public static string IfNullThenEmpty(this string str)
        {
            return str ?? String.Empty;
        }

        public static string IfNullThen(this string str, string value)
        {
            return str ?? value;
        }

        public static string IfNullOrEmptyThen(this string str, string value)
        {
            return String.IsNullOrEmpty(str) ? value : str;
        }

        public static string IfNullOrWhitespaceThen(this string str, string value)
        {
            return String.IsNullOrWhiteSpace(str) ? value : str;
        }

        public static bool IsNumericString(this string str)
        {
            return str?.Trim().All(Char.IsDigit) == true;
        }
    }
}