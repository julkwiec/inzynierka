namespace Serializer.Headers
{
    public enum TypeCode : byte
    {
        Null, // just null
        Ref, // a reference to previously read item
        Bool, // true or false
        BinaryNumber, // VLE integer, fixed decimal float (32, 64)
        DecimalNumber, // decimal float
        LocalDateTime,
        OffsetDateTime,
        TimeSpan,
        String, // byte, utf8
        List,
        Map, // a list of key-value pairs. Each item
        //Reserved
    }
}
