﻿namespace Serializer.Headers
{
    public enum StringType : byte
    {
        Bytes = 0,
        UTF8 = 1
    }
}