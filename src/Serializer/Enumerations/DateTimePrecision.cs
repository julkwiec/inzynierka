﻿namespace Serializer.Headers
{
    public enum DateTimePrecision : byte
    {
        Day = 0,
        Minute = 1,
        Second = 2,
        Tick = 3
    }
}