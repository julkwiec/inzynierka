﻿namespace Serializer.Headers
{
    public enum ReferenceType : byte
    {
        ByValue = 0,
        ByReference = 1
    }
}