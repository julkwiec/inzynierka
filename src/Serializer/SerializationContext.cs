﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Serializer.ValueSerializers;
using TypeCode = Serializer.Headers.TypeCode;
using System.IO;
using Serializer.Extensions;

namespace Serializer
{
    public class SerializationContext
    {
        private int cacheIndex = 0;
        private readonly Dictionary<int, object> deserializationCache;
        private readonly Dictionary<object, int> serializationCache;
        private readonly IDictionary<TypeCode, SerializerBase> serializers;
        
        public SerializationContext()
        {
            deserializationCache = new Dictionary<int, object>();
            serializationCache = new Dictionary<object, int>();

            serializers =
                typeof(SerializationContext)
                .GetTypeInfo()
                .Assembly
                .GetTypes()
                .Where(x => !x.GetTypeInfo().IsAbstract && typeof(SerializerBase).GetTypeInfo().IsAssignableFrom(x))
                .Select(Activator.CreateInstance)
                .Cast<SerializerBase>()
                .ToDictionary(x => x.WireType, x => x);
        }

        public SerializerBase GetSerializerForType(TypeCode type) => serializers[type];
        public SerializerBase GetSerializerForType(Type type) => serializers[TypeMap.MapTypeToCode(type)];
        public SerializerBase GetSerializerForObject(object obj) => serializers[TypeMap.MapObjectToCode(obj)];

        public int? UpdateSerializationCache(object value)
        {
            if (serializationCache.ContainsKey(value))
            {
                return serializationCache[value];
            }
            serializationCache[value] = cacheIndex;
            cacheIndex++;
            return null;
        }

        public void TryUpdateDeserializationCache(TypeCode type, object value)
        {
            if (IsCacheable(type))
            {
                var index = cacheIndex;
                cacheIndex++;
                deserializationCache[index] = value;
            }
        }

        public object GetDeserializationCacheEntry(int index) => deserializationCache[index];

        public T Deserialize<T>(Stream stream) => (T)Deserialize(stream, typeof(T));
        public object Deserialize(Stream stream, Type destinationType)
        {
            var unwrappedType = destinationType.Unwrap();
            var header = stream.ReadByteOrFail();
            var typeCode = header.ReadNumber(0, 4).CastTo<TypeCode>();
            var serializer = GetSerializerForType(typeCode);
            var result = serializer.Deserialize(header, stream, unwrappedType, this);

            var wrappedResult = result.ConvertTo(destinationType);
            return wrappedResult;
        }

        public void Serialize(Stream stream, object value)
        {
            var serializer = GetSerializerForObject(value);
            if (IsCacheable(serializer.WireType))
            {
                var id = UpdateSerializationCache(value);
                if (id.HasValue)
                {
                    var refSerializer = serializers[TypeCode.Ref];
                    refSerializer.Serialize(id.Value, stream, this);
                    return;
                }
            }
            serializer.Serialize(value, stream, this);
        }

        private bool IsCacheable(TypeCode type)
        {
            return type == TypeCode.List ||
                   type == TypeCode.Map ||
                   type == TypeCode.String ||
                   // type == TypeCode.LocalDateTime ||
                   // type == TypeCode.OffsetDateTime ||
                   // type == TypeCode.TimeSpan ||
                   // type == TypeCode.BinaryNumber ||
                   // type == TypeCode.DecimalNumber ||
                   false;
        }
    }
}
