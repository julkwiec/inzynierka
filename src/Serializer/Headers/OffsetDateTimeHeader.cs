using System;
using System.Xml;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct OffsetDateTimeHeader : IHeader
    {
        public TypeCode Type => TypeCode.OffsetDateTime;
        public bool IsBC { get; private set; }
        public bool IsOffsetNegative { get; private set; }
        public DateTimePrecision Precision { get; private set; }

        public OffsetDateTimeHeader(byte headerByte)
        {
            IsBC = headerByte.ReadBit(4);
            IsOffsetNegative = headerByte.ReadBit(5);
            Precision = (DateTimePrecision)headerByte.ReadNumber(6, 2);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteBit(4, IsBC)
                .WriteBit(5, IsOffsetNegative)
                .WriteNumber(6, 2, (int)Precision);
        }

        public static OffsetDateTimeHeader FromFields(bool isBC, bool isOffsetNegative, DateTimePrecision precision)
        {
            return new OffsetDateTimeHeader()
            {
                IsBC = isBC,
                IsOffsetNegative = isOffsetNegative,
                Precision = precision
            };
        }
    }
}