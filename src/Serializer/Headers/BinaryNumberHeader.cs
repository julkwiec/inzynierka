﻿using System;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct BinaryNumberHeader : IHeader
    {
        public TypeCode Type => TypeCode.BinaryNumber;
        public BinaryNumberType NumberType { get; private set; }

        public BinaryNumberHeader(byte headerByte)
        {
            NumberType = (BinaryNumberType)headerByte.ReadNumber(4, 2);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteNumber(4, 2, (int)NumberType);
        }

        public static BinaryNumberHeader FromFields(BinaryNumberType numberType)
        {
            return new BinaryNumberHeader()
            {
                NumberType = numberType
            };
        }
    }

    public enum BinaryNumberType : byte
    {
        PositiveVLEInteger = 0,
        NegativeVLEInteger = 1,
        Float32 = 2,
        Float64 = 3
    }
}