﻿using System;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct ListHeader : IHeader
    {
        public TypeCode Type => TypeCode.List;

        public ListHeader(byte headerByte)
        {
        }

        public byte GetValue()
        {
            return Type.AsHeader();
        }

        public static ListHeader FromFields()
        {
            return new ListHeader()
            {
            };
        }
    }
}