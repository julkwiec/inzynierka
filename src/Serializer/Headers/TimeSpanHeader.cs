using Serializer.Extensions;

namespace Serializer.Headers
{
    struct TimeSpanHeader : IHeader
    {
        public TypeCode Type => TypeCode.TimeSpan;
        public bool IsNegative { get; private set; }
        public DateTimePrecision Precision { get; private set; }

        public TimeSpanHeader(byte headerByte)
        {
            IsNegative = headerByte.ReadBit(4);
            Precision = (DateTimePrecision) headerByte.ReadNumber(5, 2);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteBit(4, IsNegative)
                .WriteNumber(5, 2, (int)Precision);
        }

        public static TimeSpanHeader FromFields(bool isNegative, DateTimePrecision precision)
        {
            return new TimeSpanHeader()
            {
                IsNegative = isNegative,
                Precision = precision
            };
        }
    }
}
