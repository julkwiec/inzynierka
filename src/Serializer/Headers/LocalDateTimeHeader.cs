using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct LocalDateTimeHeader : IHeader
    {
        public TypeCode Type => TypeCode.LocalDateTime;
        public bool IsBC { get; private set; }
        public DateTimePrecision Precision { get; private set; }

        public LocalDateTimeHeader(byte headerByte)
        {
            IsBC = headerByte.ReadBit(4);
            Precision = (DateTimePrecision)headerByte.ReadNumber(6, 2);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteBit(4, IsBC)
                .WriteNumber(6, 2, (int)Precision);
        }

        public static LocalDateTimeHeader FromFields(bool isBC, DateTimePrecision precision)
        {
            return new LocalDateTimeHeader()
            {
                IsBC = isBC,
                Precision = precision
            };
        }
    }
}