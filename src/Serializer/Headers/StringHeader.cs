﻿using Serializer.Extensions;

namespace Serializer.Headers
{
    struct StringHeader : IHeader
    {
        public TypeCode Type => TypeCode.String;
        public StringType StringType { get; private set; }

        public StringHeader(byte headerByte)
        {
            StringType = (StringType)headerByte.ReadNumber(4, 2);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteNumber(4, 2, (int)StringType);
        }

        public static StringHeader FromFields(StringType stringType)
        {
            return new StringHeader()
            {
                StringType = stringType
            };
        }
    }
}