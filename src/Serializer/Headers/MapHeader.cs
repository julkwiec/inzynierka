﻿using Serializer.Extensions;
using System;

namespace Serializer.Headers
{
    struct MapHeader : IHeader
    {
        public TypeCode Type => TypeCode.Map;

        public MapHeader(byte headerByte)
        {
        }

        public byte GetValue()
        {
            return Type.AsHeader();
        }

        public static MapHeader FromFields()
        {
            return new MapHeader();
        }
    }
}