﻿using System;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct RefHeader : IHeader
    {
        public TypeCode Type => TypeCode.Ref;
        public ReferenceType ReferenceType => ReferenceType.ByReference;
        //public ReferenceType ReferenceType { get; private set; }

        public RefHeader(byte headerByte)
        {
            //ReferenceType = headerByte.ReadBit(4) ? ReferenceType.ByValue : ReferenceType.ByReference;
        }

        public byte GetValue()
        {
            return Type.AsHeader();
                //.WriteBit(4, ReferenceType == ReferenceType.ByValue);
        }

        public static RefHeader FromFields(ReferenceType referenceType)
        {
            if (!referenceType.IsDefined()) throw new ArgumentException("Invalid argument", nameof(referenceType));
            return new RefHeader()
            {
                //ReferenceType = referenceType
            };
        }
    }
}