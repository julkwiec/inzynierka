﻿using System;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct DecimalNumberHeader : IHeader
    {
        public TypeCode Type => TypeCode.DecimalNumber;
        public bool IsMantissaNegative { get; private set; }
        public bool IsExponentNegative { get; private set; }

        public DecimalNumberHeader(byte headerByte)
        {
            IsMantissaNegative = headerByte.ReadBit(4);
            IsExponentNegative = headerByte.ReadBit(5);
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteBit(4, IsMantissaNegative)
                .WriteBit(5, IsExponentNegative);
        }

        public static DecimalNumberHeader FromFields(bool isMantissaNegative, bool isExponentNegative)
        {
            return new DecimalNumberHeader()
            {
                IsMantissaNegative = isMantissaNegative,
                IsExponentNegative = isExponentNegative
            };
        }
    }
}