﻿using System;
using Serializer.Extensions;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    struct BoolHeader : IHeader
    {
        public TypeCode Type => TypeCode.Bool;
        public bool Value { get; private set; }

        public BoolHeader(byte headerByte)
        {
            var dataField = headerByte.ReadNumber(4, 4);
            if (dataField == 0)
            {
                Value = false;
            }
            else if (dataField == 1)
            {
                Value = true;
            }
            else
            {
                throw new ArgumentException("Invalid data field");
            }
        }

        public byte GetValue()
        {
            return Type.AsHeader()
                .WriteBit(7, Value)
                .WriteNumber(4, 3, 0);
        }

        public static BoolHeader FromFields(bool value)
        {
            return new BoolHeader()
            {
                Value = value
            };
        }
    }
}