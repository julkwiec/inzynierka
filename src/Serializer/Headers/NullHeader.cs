﻿using Serializer.Extensions;
using System;

namespace Serializer.Headers
{
    struct NullHeader : IHeader
    {
        public TypeCode Type => TypeCode.Null;

        public NullHeader(byte headerByte)
        {
            if (headerByte != 0) throw new ArgumentException("Invalid header");
        }

        public byte GetValue()
        {
            return Type.AsHeader();
        }
    }
}