﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using TypeCode = Serializer.Headers.TypeCode;

namespace Serializer.Headers
{
    interface IHeader
    {
        TypeCode Type { get; }
        byte GetValue();
    }
}
