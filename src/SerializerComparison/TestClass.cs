﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace SerializerComparison
{
    [ProtoContract]
    public class TestClass
    {
        [ProtoMember(1)]
        public DateTime? Prop1 { get; set; }
        [ProtoMember(2)]
        public int Prop2 { get; set; }
        [ProtoMember(3)]
        public List<string> Prop3 { get; set; }
        [ProtoMember(4)]
        public List<string> Prop6 { get; set; }
        [ProtoMember(5)]
        public TestClass2 Prop4 { get; set; }
        [ProtoMember(6)]
        public TestClass2 Prop5 { get; set; }

        public static TestClass CreateInstance()
        {
            var item2 = TestClass2.CreateInstance();
            var list = new List<string>()
                {
                    "Hello world",
                    "Hello world",
                    "Hello world",
                    "Hello world",
                    "12345qwerty"
                };
            for(int i = 0; i < 20; i++)
            {
                list.Add(Guid.NewGuid().ToString());
            }
            return new TestClass()
            {
                Prop1 = DateTime.Now,
                Prop2 = 1234567,
                Prop3 = list,
                Prop4 = item2,
                Prop5 = item2,
            };
        }

        public override bool Equals(object obj)
        {
            var obj2 = obj as TestClass;
            if (obj2 == null) return false;
            if (this.Prop1 != obj2.Prop1) return false;
            if (this.Prop2 != obj2.Prop2) return false;

            if ((this.Prop3 == null) != (obj2.Prop3 == null)) return false;
            if (this.Prop3 == null) return false;
            if (this.Prop3.Count != obj2.Prop3.Count) return false;
            for(int i = 0; i < this.Prop3.Count; i++)
            {
                if (!Object.Equals(this.Prop3[i], obj2.Prop3[i])) return false;
            }

            if (!Object.Equals(this.Prop4, obj2.Prop4)) return false;
            if (!Object.Equals(this.Prop5, obj2.Prop5)) return false;

            if ((this.Prop6 == null) != (obj2.Prop6 == null)) return false;
            if (this.Prop6 == null) return false;
            if (this.Prop6.Count != obj2.Prop6.Count) return false;
            for (int i = 0; i < this.Prop6.Count; i++)
            {
                if (!Object.Equals(this.Prop6[i], obj2.Prop6[i])) return false;
            }

            return true;
        }
    }
}
