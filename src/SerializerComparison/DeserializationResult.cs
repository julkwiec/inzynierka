﻿using System;

namespace SerializerComparison
{
    class DeserializationResult<T>
    {
        public string Method { get; set; }
        public TimeSpan Duration { get; set; }
        public T Result { get; set; }
    }
}
