﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace SerializerComparison
{
    [ProtoContract]
    public class TestClass2
    {
        [ProtoMember(1)]
        public DateTime? Prop1 { get; set; }
        [ProtoMember(2)]
        public int Prop2 { get; set; }
        [ProtoMember(3)]
        public List<string> Prop3 { get; set; }
        [ProtoMember(4)]
        public TimeSpan Prop4 { get; set; }

        public static TestClass2 CreateInstance()
        {
            return new TestClass2()
            {
                Prop1 = DateTime.Now,
                Prop2 = 1234567,
                Prop3 = new List<string>()
                {
                    "Hello world",
                    "12345qwerty"
                },
                Prop4 = TimeSpan.FromDays(123456)
            };
        }

        public override bool Equals(object obj)
        {
            var obj2 = obj as TestClass2;
            if (obj2 == null) return false;
            if (this.Prop1 != obj2.Prop1) return false;
            if (this.Prop2 != obj2.Prop2) return false;
            if ((this.Prop3 == null) != (obj2.Prop3 == null)) return false;
            if (this.Prop3 == null) return false;
            if (this.Prop3.Count != obj2.Prop3.Count) return false;
            if (this.Prop4 != obj2.Prop4) return false;
            for (int i = 0; i < this.Prop3.Count; i++)
            {
                if (!Object.Equals(this.Prop3[i], obj2.Prop3[i])) return false;
            }
            return true;
        }
    }
}
