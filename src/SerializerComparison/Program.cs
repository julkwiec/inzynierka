﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.IO.Compression;
using System.Linq;

namespace SerializerComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            int warmupCycles = 100;
            int iterations = 1000;
            var items = Enumerable.Range(0, 6).Select(x => TestClass.CreateInstance()).ToArray();
            //items = Enumerable.Range(0, 5).SelectMany(x => items).ToArray();

            RunAndLogAllMethods(items, warmupCycles, iterations);

            Console.ReadLine();
        }

        static void RunAndLogAllMethods<T>(T obj, int warmupCycles, int iterations)
        {
            var methods = new[]
            {
                Tuple.Create(SerializationMethod.Json, false),
                Tuple.Create(SerializationMethod.Json, true),
                Tuple.Create(SerializationMethod.Xml, false),
                Tuple.Create(SerializationMethod.Xml, true),
                Tuple.Create(SerializationMethod.Protobuf, false),
                Tuple.Create(SerializationMethod.Protobuf, true),
                Tuple.Create(SerializationMethod.GDNSerializer, false),
                 Tuple.Create(SerializationMethod.GDNSerializer, true),
            };

            foreach(var method in methods)
            {
                RunAndLog(obj, method.Item1, method.Item2, warmupCycles, iterations);
            }
        }

        static void RunAndLog<T>(T obj, SerializationMethod method, bool useGzip, int warmupCycles, int iterations)
        {
            var serializer = new Serializer(method, useGzip);
            var serializationResult = serializer.Serialize<T>(obj, warmupCycles, iterations);
            var deserializationResult = serializer.Deserialize<T>(serializationResult.Result, warmupCycles, iterations);
            LogLine(
                serializationResult.Method, 
                serializationResult.Result.Length, 
                serializationResult.Duration.TotalMilliseconds, 
                deserializationResult.Duration.TotalMilliseconds);
        }

        static void LogLine(string name, int length, double serializationTime, double deserializationTime)
        {
            Console.WriteLine($"{name.PadLeft(20)}: {length:000} bytes, write: {serializationTime:0.0000}ms, read: {deserializationTime:0.0000}ms");
        }
    }
}