﻿using System;

namespace SerializerComparison
{
    class SerializationResult
    {
        public string Method { get; set; }
        public TimeSpan Duration { get; set; }
        public byte[] Result { get; set; }
    }
}
