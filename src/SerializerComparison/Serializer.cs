﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;
using Newtonsoft.Json;
using ProtoBuf;
using System.Xml;
using System.Xml.Serialization;
using Serializer;
using System.Diagnostics;

namespace SerializerComparison
{
    class Serializer
    {
        private SerializationMethod method;
        private bool useGzip;

        public Serializer(SerializationMethod method, bool useGzip)
        {
            this.method = method;
            this.useGzip = useGzip;
        }

        public DeserializationResult<T> Deserialize<T>(byte[] data, int warmupCycles = 1, int count = 1)
        {
            var deserializer = GetDeserializer<T>();
            var result = new DeserializationResult<T>()
            {
                Method = GetMethod()
            };

            using (var ms = new MemoryStream(data))
            {
                result.Result = deserializer(ms);

                for (int i = 0; i < warmupCycles; i++)
                {
                    ms.Position = 0;
                    deserializer(ms);
                }

                var sw = Stopwatch.StartNew();
                for (int i = 0; i < count; i++)
                {
                    ms.Position = 0;
                    deserializer(ms);
                }
                sw.Stop();
                result.Duration = TimeSpan.FromTicks(sw.ElapsedTicks / count);
            }
            return result;
        }

        public SerializationResult Serialize<T>(T obj, int warmupCycles = 1, int count = 1)
        {
            var serializer = GetSerializer<T>();
            var result = new SerializationResult()
            {
                Method = GetMethod()
            };
            
            using (var ms = new MemoryStream())
            {
                serializer(obj, ms);
                result.Result = ms.ToArray();

                for (int i = 0; i < warmupCycles; i++)
                {
                    ms.Position = 0;
                    serializer(obj, ms);
                }

                var sw = Stopwatch.StartNew();
                for (int i = 0; i < count; i++)
                {
                    ms.Position = 0;
                    serializer(obj, ms);
                }
                sw.Stop();
                result.Duration = TimeSpan.FromTicks(sw.ElapsedTicks / count);
            }
            return result;
        }

        private string GetMethod() => method.ToString() + (useGzip ? " + GZip" : "");

        private Action<T, Stream> GetSerializer<T>()
        {
            Action<T, Stream> serialize = null;
            switch (method)
            {
                case SerializationMethod.Json:
                    {
                        serialize = ((obj, stream) =>
                        {
                            var serializer = JsonSerializer.Create(new JsonSerializerSettings()
                            {
                                Formatting = Formatting.None
                            });
                            using (var writer = new StreamWriter(stream, Encoding.UTF8, 4096, true))
                            using (var jsonWriter = new JsonTextWriter(writer))
                            {
                                serializer.Serialize(jsonWriter, obj);
                            }
                        });
                        break;
                    }
                case SerializationMethod.Xml:
                    {
                        serialize = ((obj, stream) =>
                        {
                            var settings = new XmlWriterSettings();
                            settings.Encoding = System.Text.Encoding.UTF8;
                            settings.Indent = false;
                            settings.NewLineChars = String.Empty;
                            settings.NewLineHandling = NewLineHandling.Replace;
                            using (var writer = XmlWriter.Create(stream, settings))
                            {
                                var serializer = new XmlSerializer(typeof(T));
                                serializer.Serialize(writer, obj);
                            }
                        });
                        break;
                    }
                case SerializationMethod.Protobuf:
                    {
                        serialize = ((obj, stream) =>
                        {
                            ProtoBuf.Serializer.Serialize(stream, obj);
                        });
                        break;
                    }
                case SerializationMethod.GDNSerializer:
                    {
                        serialize = ((obj, stream) =>
                        {
                            GDNSerializer.Serialize(stream, obj);
                        });
                        break;
                    }
            }
            if (useGzip)
            {
                return new Action<T, Stream>((obj, stream) =>
                {
                    using (var gzip = new GZipStream(stream, CompressionLevel.Optimal, true))
                    {
                        serialize(obj, gzip);
                    }
                });
            }
            else
            {
                return serialize;
            }
        }

        private Func<Stream, T> GetDeserializer<T>()
        {
            Func<Stream, T> deserialize = null;
            switch (method)
            {
                case SerializationMethod.Json:
                    {
                        deserialize = (x =>
                        {
                            var serializer = JsonSerializer.Create(new JsonSerializerSettings()
                            {
                                Formatting = Formatting.None
                            });
                            using (var reader = new StreamReader(x, Encoding.UTF8, false, 4096, true))
                            using (var jsonReader = new JsonTextReader(reader))
                            {
                                return serializer.Deserialize<T>(jsonReader);
                            }
                        });
                        break;
                    }
                case SerializationMethod.GDNSerializer:
                    {
                        deserialize = GDNSerializer.Deserialize<T>;
                        break;
                    }
                case SerializationMethod.Protobuf:
                    {
                        deserialize = ProtoBuf.Serializer.Deserialize<T>;
                        break;
                    }
                case SerializationMethod.Xml:
                    {
                        deserialize = (x =>
                        {
                            using (var reader = XmlReader.Create(x))
                            {
                                var serializer = new XmlSerializer(typeof(T));
                                return (T)serializer.Deserialize(reader);
                            }
                        });
                        break;
                    }
            }
            if (useGzip)
            {
                return new Func<Stream, T>(x =>
                {
                    using (var gzip = new GZipStream(x, CompressionMode.Decompress, true))
                    {
                        return deserialize(gzip);
                    }
                });
            }
            else
            {
                return deserialize;
            }
        }
    }
}
