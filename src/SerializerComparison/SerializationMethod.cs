﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializerComparison
{
    enum SerializationMethod
    {
        Json,
        Xml,
        Protobuf,
        GDNSerializer
    }
}
