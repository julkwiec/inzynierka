﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Serializer.Extensions;

namespace Serializer.Tests
{
    public class MathTests
    {
        [Fact]
        public void TestDivideRoundUp()
        {
            Assert.Equal(0, 0.DivideRoundUp(8));
            Assert.Equal(1, 1.DivideRoundUp(8));
            Assert.Equal(1, 2.DivideRoundUp(8));
            Assert.Equal(1, 7.DivideRoundUp(8));
            Assert.Equal(1, 8.DivideRoundUp(8));
            Assert.Equal(2, 9.DivideRoundUp(8));
            Assert.Equal(2, 10.DivideRoundUp(8));
            Assert.Equal(2, 11.DivideRoundUp(8));
            Assert.Equal(2, 15.DivideRoundUp(8));
            Assert.Equal(2, 16.DivideRoundUp(8));
            Assert.Equal(3, 17.DivideRoundUp(8));
        }
    }
}
