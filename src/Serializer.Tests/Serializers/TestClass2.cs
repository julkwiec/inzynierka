﻿using System;
using System.Collections.Generic;

namespace Serializer.Tests.Serializers
{
    public class TestClass2
    {
        public DateTime? Prop1 { get; set; }
        public int Prop2 { get; set; }
        public List<string> Prop3 { get; set; }

        public static TestClass2 CreateInstance()
        {
            return new TestClass2()
            {
                Prop1 = DateTime.Now,
                Prop2 = 1234567,
                Prop3 = new List<string>()
                {
                    "Hello world",
                    "12345qwerty",
                    DateTime.Now.ToString()
                }
            };
        }

        public override bool Equals(object obj)
        {
            var obj2 = obj as TestClass2;
            if (obj2 == null) return false;
            if (this.Prop1 != obj2.Prop1) return false;
            if (this.Prop2 != obj2.Prop2) return false;
            if ((this.Prop3 == null) != (obj2.Prop3 == null)) return false;
            if (this.Prop3 == null) return false;
            if (this.Prop3.Count != obj2.Prop3.Count) return false;
            for (int i = 0; i < this.Prop3.Count; i++)
            {
                if (!Object.Equals(this.Prop3[i], obj2.Prop3[i])) return false;
            }
            return true;
        }
    }
}
