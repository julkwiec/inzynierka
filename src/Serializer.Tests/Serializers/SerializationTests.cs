﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serializer.Extensions;
using Xunit;
using System.IO;
using NodaTime;

namespace Serializer.Tests.Serializers
{

    public class SerializationTests
    {
        [Fact]
        public void TestDateTime()
        {
            var value = new DateTime?[] { new DateTime(2000, 03, 06, 10, 20, 30, 500), DateTime.Now, DateTime.MinValue, DateTime.MaxValue, null };
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, value);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<DateTime?[]>(ms);
                //var item1 = value[0].Value;
                //var item2 = result[0].Value;
                Assert.Equal(value, result);
            }
        }

        [Theory]
        [MemberData(nameof(TestTimeSpanData))]
        public void TestTimeSpan(TimeSpan value)
        {
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, value);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<TimeSpan>(ms);
                Assert.Equal(value, result);
            }
        }

        public static IEnumerable<object[]> TestTimeSpanData() => new[] {
            new object[]{ TimeSpan.MinValue },
            new object[]{ TimeSpan.Zero },
            new object[]{ TimeSpan.MaxValue }
        };

        [Theory]
        [MemberData(nameof(TestBooleanData))]
        public void TestBoolean(bool value)
        {
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, value);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<bool>(ms);
                Assert.Equal(value, result);
            }
        }

        public static IEnumerable<object[]> TestBooleanData() => new[] {
            new object[]{ (bool?) true },
            new object[]{ (bool?) false },
            new object[]{ (bool?) null }
        };

        [Theory]
        [MemberData(nameof(TestDecimalNumberData))]
        public void TestDecimalNumber(decimal? value)
        {
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, value);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<decimal?>(ms);
                Assert.Equal(value, result);
            }
        }

        public static IEnumerable<object[]> TestDecimalNumberData => new[] {
            new object[]{ (decimal?) decimal.MinValue },
            new object[]{ (decimal?) decimal.MinusOne },
            new object[]{ (decimal?) decimal.Zero },
            new object[]{ (decimal?) decimal.One },
            new object[]{ (decimal?) decimal.MaxValue },
            new object[]{ (decimal?) null }
        };

        private void ExecuteTest(object value)
        {
            var type = value?.GetType() ?? typeof(object);
            var bytes = GDNSerializer.Serialize(value);
            var result = GDNSerializer.Deserialize(bytes, type);
            Assert.Equal(value, result);
        }

        [Theory]
        [MemberData(nameof(GeneralTestData_Float))]
        public void GeneralTest_Float(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Double))]
        public void GeneralTest_Double(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Byte))]
        public void GeneralTest_Byte(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Sbyte))]
        public void GeneralTest_Sbyte(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Int))]
        public void GeneralTest_Int(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Ulong))]
        public void GeneralTest_Ulong(object value) => ExecuteTest(value);

        [Theory]
        [MemberData(nameof(GeneralTestData_Long))]
        public void GeneralTest_Long(object value) => ExecuteTest(value);

        public static IEnumerable<object[]> GeneralTestData_Float => new[] {
            new object[]{ (float?) float.NegativeInfinity },
            new object[]{ (float?) float.MinValue },
            new object[]{ (float?) float.Epsilon },
            new object[]{ (float?) float.MaxValue },
            new object[]{ (float?) float.PositiveInfinity },
            new object[]{ (float?) float.NaN },
            new object[]{ (float?)  1.0F },
            new object[]{ (float?)  0.1F },
            new object[]{ (float?)  0.0F },
            new object[]{ (float?) -0.1F },
            new object[]{ (float?) -1.0F },
            new object[]{ (float?) null }
        };

        public static IEnumerable<object[]> GeneralTestData_Double => new[] {
            new object[]{ (double?) double.NegativeInfinity },
            new object[]{ (double?) double.MinValue },
            new object[]{ (double?) double.Epsilon },
            new object[]{ (double?) double.MaxValue },
            new object[]{ (double?) double.PositiveInfinity },
            new object[]{ (double?) double.NaN },
            new object[]{ (double?)  1.0 },
            new object[]{ (double?)  0.1 },
            new object[]{ (double?)  0.0 },
            new object[]{ (double?) -0.1 },
            new object[]{ (double?) -1.0 },
            new object[]{ (double?) null },
        };

        public static IEnumerable<object[]> GeneralTestData_Byte => new[] {
            new object[]{ (byte?) byte.MinValue },
            new object[]{ (byte?) byte.MaxValue },
            new object[]{ (byte?) byte.MinValue + 1 },
            new object[]{ (byte?) byte.MaxValue - 1 },
            new object[]{ (byte?) 1 },
        };

        public static IEnumerable<object[]> GeneralTestData_Sbyte => new[] {
            new object[]{ (sbyte?) sbyte.MinValue },
            new object[]{ (sbyte?) sbyte.MaxValue },
            new object[]{ (sbyte?) sbyte.MinValue + 1 },
            new object[]{ (sbyte?) sbyte.MaxValue - 1},
            new object[]{ (sbyte?)  1 },
            new object[]{ (sbyte?)  0 },
            new object[]{ (sbyte?) -1 },
        };

        public static IEnumerable<object[]> GeneralTestData_Int => new[] {
            new object[]{ (int) Int32.MinValue },
            new object[]{ (int) Int32.MinValue + 1 },
            new object[]{ (int) Int32.MinValue + 2 },
            new object[]{ (int) Int32.MinValue + 3 },
            new object[]{ (int) Int32.MinValue + 4 },
            new object[]{ (int) -4 },
            new object[]{ (int) -3 },
            new object[]{ (int) -2 },
            new object[]{ (int) -1 },
            new object[]{ (int) 0 },
            new object[]{ (int) 1 },
            new object[]{ (int) 2 },
            new object[]{ (int) 3 },
            new object[]{ (int) 4 },
            new object[]{ (int) Int32.MaxValue - 4 },
            new object[]{ (int) Int32.MaxValue - 3 },
            new object[]{ (int) Int32.MaxValue - 2 },
            new object[]{ (int) Int32.MaxValue - 1 },
            new object[]{ (int) Int32.MaxValue },
        };

        public static IEnumerable<object[]> GeneralTestData_Ulong => new[] {
            new object[]{ (ulong?) ulong.MinValue },
            new object[]{ (ulong?) ulong.MaxValue },
            new object[]{ (ulong?) ulong.MinValue + 1 },
            new object[]{ (ulong?) ulong.MaxValue - 1 },
            new object[]{ (ulong?)  1 },
            new object[]{ (ulong?)  0 },
            new object[]{ (ulong?)  null },
        };

        public static IEnumerable<object[]> GeneralTestData_Long => new[] {
            new object[]{ (long?) long.MinValue },
            new object[]{ (long?) long.MaxValue },
            new object[]{ (long?) long.MinValue + 1 },
            new object[]{ (long?) long.MaxValue - 1},
            new object[]{ (long?)  1 },
            new object[]{ (long?)  0 },
            new object[]{ (long?) -1 },
            new object[]{ (long?) null },
        };
    }
}
