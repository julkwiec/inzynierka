﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Serializer.Tests.Serializers
{
    public class MapSerialization
    {
        [Fact]
        public void DictionaryTest()
        {
            var dict = new Dictionary<int, DateTime>
            {
                { 1, DateTime.MinValue },
                { 2, DateTime.Now },
                { 3, DateTime.MaxValue },
            };
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, dict);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<IDictionary<int, DateTime>>(ms);
                Assert.Equal(dict, result);
            }            
        }

        [Fact]
        public void ObjectTest()
        {
            var obj = TestClass.CreateInstance();
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                var ctx = new SerializationContext();
                ctx.Serialize(ms, obj);
                buffer = ms.ToArray();
            }
            using (var ms = new MemoryStream(buffer))
            {
                var ctx = new SerializationContext();
                var result = ctx.Deserialize<TestClass>(ms);
                Assert.True(obj.Equals(result));
            }
            var utf8Json = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None);
            var prettyJson = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);
            var jsonBytes = System.Text.Encoding.UTF8.GetBytes(utf8Json);
            Assert.True(buffer.Length <= jsonBytes.Length);
        }

        [Fact]
        public void CircularReferenceTest()
        {
            var item1 = A.CreateInstance();
            var item2 = A.CreateInstance();
            item1.Reference = item2;
            item2.Reference = item1;

            var bytes = GDNSerializer.Serialize(item1);
            var newItem1 = GDNSerializer.Deserialize<A>(bytes);
            Assert.True(newItem1 == newItem1?.Reference?.Reference);
        }
    }

    class A
    {
        public int Index { get; set; }
        public string Value { get; set; }
        public A Reference { get; set; }
        static int counter = 0;
        public static A CreateInstance()
        {
            counter++;
            return new A()
            {
                Value = Guid.NewGuid().ToString(),
                Index = counter
            };
        }

        public override string ToString()
        {
            return $"Index: {Index}, {Value}";
        }
    }
}
