﻿using System;
using System.Collections.Generic;

namespace Serializer.Tests.Serializers
{
    public class TestClass
    {
        public DateTime? Prop1 { get; set; }
        public int Prop2 { get; set; }
        public List<string> Prop3 { get; set; }
        public List<string> Prop6 { get; set; }
        public TestClass2 Prop4 { get; set; }
        public TestClass2 Prop5 { get; set; }

        public static TestClass CreateInstance()
        {
            var list = new List<string>()
                {
                    "Hello world",
                    "Hello world",
                    Guid.NewGuid().ToString(),
                    "Hello world",
                    "12345qwerty"
                };
            return new TestClass()
            {
                Prop1 = DateTime.Now,
                Prop2 = new System.Random().Next(),
                Prop3 = list,
                Prop4 = TestClass2.CreateInstance(),
                Prop5 = TestClass2.CreateInstance(),
                Prop6 = list,
            };
        }

        public override bool Equals(object obj)
        {
            var obj2 = obj as TestClass;
            if (obj2 == null) return false;
            if (this.Prop1 != obj2.Prop1) return false;
            if (this.Prop2 != obj2.Prop2) return false;

            if ((this.Prop3 == null) != (obj2.Prop3 == null)) return false;
            if (this.Prop3 == null) return false;
            if (this.Prop3.Count != obj2.Prop3.Count) return false;
            for(int i = 0; i < this.Prop3.Count; i++)
            {
                if (!Object.Equals(this.Prop3[i], obj2.Prop3[i])) return false;
            }

            if (!Object.Equals(this.Prop4, obj2.Prop4)) return false;
            if (!Object.Equals(this.Prop5, obj2.Prop5)) return false;

            if ((this.Prop6 == null) != (obj2.Prop6 == null)) return false;
            if (this.Prop6 == null) return false;
            if (this.Prop6.Count != obj2.Prop6.Count) return false;
            for (int i = 0; i < this.Prop6.Count; i++)
            {
                if (!Object.Equals(this.Prop6[i], obj2.Prop6[i])) return false;
            }

            return true;
        }
    }
}
