﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serializer.Extensions;
using Xunit;

namespace Serializer.Tests
{
    public class BitReaderTests
    {
        [Fact]
        public void BitReaderShouldThrowIfOutOfRange()
        {
            byte value = 0xBB;
            
            for (int i = -10; i < 20; i++)
            {
                if (i < 0 || i > 7)
                {
                    Assert.Throws<ArgumentOutOfRangeException>(() =>
                    {
                        value.ReadBit(i);
                    });
                }
                else
                {
                    value.ReadBit(i);
                }
            }
        }

        [Fact]
        public void BitReaderShouldThrowExceptionIfOutOfRange()
        {
            byte value = 0xBB;

            for (int start = -10; start < 20; start++)
            for (int count = -10; count < 20; count++)
            {
                if (start >= 0 && start <= 7 && count > 0 && (start + count) <= 8)
                {
                    value.ReadNumber(start, count);
                }
                else
                {
                    Assert.Throws<ArgumentOutOfRangeException>(() =>
                    {
                        value.ReadNumber(start, count);
                    });
                }
            }
        }

        [Fact]
        public void BitReaderShouldReadValidBit()
        {
            byte value = 0xAA;

            Assert.Equal(true,  value.ReadBit(0));
            Assert.Equal(false, value.ReadBit(1));
            Assert.Equal(true,  value.ReadBit(2));
            Assert.Equal(false, value.ReadBit(3));
            Assert.Equal(true,  value.ReadBit(4));
            Assert.Equal(false, value.ReadBit(5));
            Assert.Equal(true,  value.ReadBit(6));
            Assert.Equal(false, value.ReadBit(7));
        }

        [Fact]
        public void BitReaderShouldReadValidNumber()
        {
            byte value = 0xBB;

            Assert.Equal(0x0B, value.ReadNumber(0, 4));
            Assert.Equal(0x0B, value.ReadNumber(4, 4));
        }

        [Fact]
        public void MakeByteTest()
        {
            Assert.Equal(MakeByte("00000001"), (byte)0x01);
            Assert.Equal(MakeByte("10000001"), (byte)0x81);
            Assert.Equal(MakeByte("00000000"), (byte)0x00);
            Assert.Equal(MakeByte("11111111"), (byte)0xFF);
            Assert.Equal(MakeByte("00001111"), (byte)0x0F);
            Assert.Equal(MakeByte("11110000"), (byte)0xF0);
            Assert.Equal(MakeByte("10101010"), (byte)0xAA);
            Assert.Equal(MakeByte("01010101"), (byte)0x55);

            Assert.Equal("00000001", MakeString(0x01));
            Assert.Equal("10000001", MakeString(0x81));
            Assert.Equal("00000000", MakeString(0x00));
            Assert.Equal("11111111", MakeString(0xFF));
            Assert.Equal("00001111", MakeString(0x0F));
            Assert.Equal("11110000", MakeString(0xF0));
            Assert.Equal("10101010", MakeString(0xAA));
            Assert.Equal("01010101", MakeString(0x55));


        }

        [Fact]
        public void WriteNumberToByteTest()
        {
            Assert.Equal(
                MakeString(MakeByte("10000011")),
                MakeString(MakeByte("10000000").WriteNumber(6, 2, MakeByte("00000011"))));

            Assert.Equal(
                MakeString(MakeByte("10000110")),
                MakeString(MakeByte("10000000").WriteNumber(5, 2, MakeByte("00000011"))));

            Assert.Equal(
                MakeString(MakeByte("10000000")),
                MakeString(MakeByte("11111111").WriteNumber(1, 7, MakeByte("00000000"))));

            Assert.Equal(
                MakeString(MakeByte("10000000")),
                MakeString(MakeByte("00000000").WriteNumber(0, 1, MakeByte("00000001"))));

            Assert.Equal(
                MakeString(MakeByte("00000001")),
                MakeString(MakeByte("00000000").WriteNumber(7, 1, MakeByte("00000001"))));

            Assert.Equal(
                MakeString(MakeByte("01000000")),
                MakeString(MakeByte("00000000").WriteNumber(1, 1, MakeByte("00000001"))));

            Assert.Equal(
                MakeString(MakeByte("00000010")),
                MakeString(MakeByte("00000000").WriteNumber(6, 1, MakeByte("00000001"))));

            Assert.Equal(
                MakeString(MakeByte("11000000")),
                MakeString(MakeByte("00000000").WriteNumber(0, 2, MakeByte("00000011"))));

            Assert.Equal(
                MakeString(MakeByte("00000011")),
                MakeString(MakeByte("00000000").WriteNumber(6, 2, MakeByte("00000011"))));

            Assert.Equal(
                MakeString(MakeByte("00000000")),
                MakeString(MakeByte("11111111").WriteNumber(0, 8, MakeByte("00000000"))));

            Assert.Equal(
                MakeString(MakeByte("11111111")),
                MakeString(MakeByte("00000000").WriteNumber(0, 8, MakeByte("11111111"))));
        }

        private byte MakeByte(string str)
        {
            if (str == null || str.Length != 8 || str.Any(x => x != '0' && x != '1'))
            {
                throw new Exception();
            }
            byte result = 0;
            for (int i = 0; i < 8; i++)
            {
                if (str[i] == '1')
                {
                    result |= (byte)(0x01 << (7-i));
                }
            }
            return result;
        }

        private string MakeString(byte b)
        {
            string result = "";
            for (int i = 0; i < 8; i++)
            {
                int value = (b >> i) & 0x01;
                result = value + result;
            }
            return result;
        }
    }
}
