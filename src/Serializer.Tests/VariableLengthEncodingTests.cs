using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using Serializer.Encoding;
using Serializer.Extensions;
using Xunit;
using Serializer.Helpers;

namespace Serializer.Tests
{
    public class VariableLengthEncodingTests
    {
        public static IEnumerable<object[]> EncodeByteData => new[]
        {
            new object[] {(byte) 0x00, new byte[] {0x00}},
            new object[] {(byte) 0x01, new byte[] {0x01}},
            new object[] {(byte) 0x7E, new byte[] {0x7E}},
            new object[] {(byte) 0x7F, new byte[] {0x7F}},
            new object[] {(byte) 0x80, new byte[] {0x81, 0x00}},
            new object[] {(byte) 0x81, new byte[] {0x81, 0x01}},
            new object[] {(byte) 0xFE, new byte[] {0x81, 0x7E}},
            new object[] {(byte) 0xFF, new byte[] {0x81, 0x7F}}
        };

        [Theory, MemberData(nameof(EncodeByteData))]
        public void EncodeByte(byte value, byte[] expected)
        {
            var actual = VariableLengthEncoding.Encode(value);
            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> EncodeUshortData => new[]
        {
            new object[] {(ushort) 0x0000, new byte[] {0x00}},
            new object[] {(ushort) 0x1234, new byte[] {0xA4, 0x34}},
            new object[] {(ushort) 0xFFFF, new byte[] {0x83, 0xFF, 0x7F}},
            new object[] {(ushort) 2017, new byte[] {0x8F, 0x61}}
        };

        [Theory, MemberData(nameof(EncodeUshortData))]
        public void EncodeUshort(ushort value, byte[] expected)
        {
            var actual = VariableLengthEncoding.Encode(value);
            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> EncodeUlongData => new[]
        {
            new object[] {(ulong) 540, new byte[] {0x84, 0x1C}},
            new object[] {(ulong) 2017, new byte[] {0x8F, 0x61}},
            new object[] {(ulong) 02, new byte[] {0x02}},
            new object[] {(ulong) 04, new byte[] {0x04}},
            new object[] {(ulong) 12, new byte[] {0x0C}},
            new object[] {(ulong) 26, new byte[] {0x1A}},
            new object[] {(ulong) 13, new byte[] {0x0D}},
            new object[] {(ulong) 134405593, new byte[] {0xC0, 0x8B, 0xBB, 0x59}},
        };

        [Theory, MemberData(nameof(EncodeUlongData))]
        public void EncodeUlong(ulong value, byte[] expected)
        {
            var actual = VariableLengthEncoding.Encode(value);
            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> EncodeIntData => new[]
        {
            new object[] {(int) 540, new byte[] {0x84, 0x1C}},
            new object[] {(int) 2017, new byte[] {0x8F, 0x61}},
            new object[] {(int) 02, new byte[] {0x02}},
            new object[] {(int) 04, new byte[] {0x04}},
            new object[] {(int) 12, new byte[] {0x0C}},
            new object[] {(int) 26, new byte[] {0x1A}},
            new object[] {(int) 13, new byte[] {0x0D}},
            new object[] {(int) 134405593, new byte[] {0xC0, 0x8B, 0xBB, 0x59}},
            new object[] {(int) Int32.MaxValue - 4, new byte[] {0x87, 0xFF, 0xFF, 0xFF, 0x7B}},
            new object[] {(int) Int32.MaxValue - 3, new byte[] {0x87, 0xFF, 0xFF, 0xFF, 0x7C}},
            new object[] {(int) Int32.MaxValue - 2, new byte[] {0x87, 0xFF, 0xFF, 0xFF, 0x7D}},
            new object[] {(int) Int32.MaxValue - 1, new byte[] {0x87, 0xFF, 0xFF, 0xFF, 0x7E}},
            new object[] {(int) Int32.MaxValue    , new byte[] {0x87, 0xFF, 0xFF, 0xFF, 0x7F}},
        };

        [Theory, MemberData(nameof(EncodeIntData))]
        public void EncodeInt(int value, byte[] expected)
        {
            var actual = VariableLengthEncoding.Encode(value);
            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> EncodeBigIntegerData => new[]
        {
            new object[] {BigInteger.Parse("123456"), new byte[] {0x87, 0xC4, 0x40}}
        };

        [Theory, MemberData(nameof(EncodeBigIntegerData))]
        public void EncodeBigInteger(BigInteger value, byte[] expected)
        {
            var actual = VariableLengthEncoding.Encode(value);
            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> DecodeByteData => new[]
        {
            new object[] {new byte[] {0x00}, 0x00},
            new object[] {new byte[] {0x01}, 0x01},
            new object[] {new byte[] {0x7E}, 0x7E},
            new object[] {new byte[] {0x7F}, 0x7F},
            new object[] {new byte[] {0x81, 0x00}, 0x80},
            new object[] {new byte[] {0x81, 0x01}, 0x81},
            new object[] {new byte[] {0x81, 0x7E}, 0xFE},
            new object[] {new byte[] {0x81, 0x7F}, 0xFF},
        };

        [Theory, MemberData(nameof(DecodeByteData))]
        public void DecodeByteFromArray(byte[] bytes, byte expected)
        {
            var actual = VariableLengthEncoding.DecodeByte(new ByteVector(bytes));
            Assert.Equal(expected, actual);
        }

        [Theory, MemberData(nameof(DecodeByteData))]
        public void DecodeByteFromStream(byte[] bytes, byte expected)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var actual = VariableLengthEncoding.DecodeByte(stream);
                Assert.Equal(expected, actual);
            }
        }
    }
}
